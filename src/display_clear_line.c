#include <linux/i2c-dev.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){

 int file;
 char buf[17];
 memset(buf, 0x20, 17);

 if(argc < 2){
  printf("Need line.");
  return(1);
 }

 if( strcmp(argv[1], "1") == 0 ) buf[0] = 0x08;
 else if( strcmp(argv[1], "2") == 0 ) buf[0] = 0x18;
 else{
  printf("Wrong line.");
  return(1);
 }

 file = open("/dev/i2c-1", O_RDWR);

 if(file<0){
  printf("Error opening the i2c bus.");
  return(1);
 }

 if(ioctl(file, I2C_SLAVE, 0x71) < 0) {
  printf("Error changing i2c slave address.");
  return(1);
 }

 if(write(file, buf, 17) != 17) {
  printf("Error writing to device.");
 }

}
