#include <linux/i2c-dev.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){

 int i, file;
 char buf[1];
 int count = 0;

 file = open("/dev/i2c-1", O_RDWR);

 if(file<0){
  printf("Error opening the i2c bus.");
  return(1);
 }

 buf[0] = 0x00;
 ioctl(file, I2C_SLAVE, 0x51);
 if(write(file, buf, 1) != 1) 		printf("S1_NOK ");
 else if(read(file, buf, 1) != 1) 	printf("S1_NOK ");
 else if(buf[0] == 0x55)  		printf("S1_OK ");

 buf[0] = 0x00;
 ioctl(file, I2C_SLAVE, 0x52);
 if(write(file, buf, 1) != 1) 		printf("S2_NOK ");
 else if(read(file, buf, 1) != 1) 	printf("S2_NOK ");
 else if(buf[0] == 0x55)  		printf("S2_OK ");

 buf[0] = 0x00;
 ioctl(file, I2C_SLAVE, 0x61);
 if(write(file, buf, 1) != 1) 		printf("R1_NOK ");
 else if(read(file, buf, 1) != 1) 	printf("R1_NOK ");
 else if(buf[0] == 0x55)  		printf("R1_OK ");

 buf[0] = 0x00;
 ioctl(file, I2C_SLAVE, 0x71);
 if(write(file, buf, 1) != 1) 		printf("D1_NOK ");
 else if(read(file, buf, 1) != 1) 	printf("D1_NOK ");
 else if(buf[0] == 0x55)  		printf("D1_OK ");


}
