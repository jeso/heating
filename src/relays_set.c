#include <linux/i2c-dev.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){

 int file, line;
 char bufrd[1], bufen[2], buf1[2], buf2[2], buf3[2];
 int output, mode, x, y;

 if(argc < 5){
   printf("Output number, mode, x and y!\n");
   return(1);
 }

 output = strtol(argv[1], NULL, 10);
 mode = strtol(argv[2], NULL, 16);
 x = strtol(argv[3], NULL, 16);
 y = strtol(argv[4], NULL, 16);

 file = open("/dev/i2c-1", O_RDWR);

 bufen[0] = 0x04;
 bufen[1] = (1 << output);

 buf1[0] = 0x08 + output;
 buf2[0] = 0x10 + output;
 buf3[0] = 0x18 + output;

 buf1[1] = mode;
 buf2[1] = x;
 buf3[1] = y;

 if(file<0){
  printf("Error opening the i2c bus.");
  return(1);
 }

 if(ioctl(file, I2C_SLAVE, 0x61) < 0) {
  printf("Error changing i2c slave address.");
  return(1);
 }

 bufrd[0] = 0x04;
 if(write(file, bufrd, 1) != 1) {
  printf("Error writing to device.");
 }
 if(read(file, bufrd, 1) != 1) {
   printf("Error reading from device.");
 }
 bufen[1] |= bufrd[0];

 if(write(file, bufen, 2) != 2) {
  printf("Error writing to device.");
 }

 if(write(file, buf1, 2) != 2) {
  printf("Error writing to device.");
 }

 if(write(file, buf2, 2) != 2) {
  printf("Error writing to device.");
 }

 if(write(file, buf3, 2) != 2) {
  printf("Error writing to device.");
 }


}
