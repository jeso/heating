#include <linux/i2c-dev.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){

 int i, temp;
 int file;
 char buf[128];
 char addr;
 char reg, data;
 memset(buf, 0, 128);

 if(argc < 4){
   printf("Need address, register and data\n!");
   return(1);
 }

 if(argc == 4){

  if( strcmp(argv[1], "0x51" ) == 0){
    addr = 0x51;
  }
  else if( strcmp(argv[1], "0x52" ) == 0){
    addr = 0x52;
  }
  else if( strcmp(argv[1], "0x61" ) == 0){
    addr = 0x61;
  }
  else if( strcmp(argv[1], "0x62" ) == 0){
    addr = 0x62;
  }
  else if( strcmp(argv[1], "0x71" ) == 0){
    addr = 0x71;
  }
  else if( strcmp(argv[1], "0x72" ) == 0){
    addr = 0x72;
  }
  else{
    printf("Unknown address");
    return(1);
  }

  temp = strtol(argv[2], NULL, 16);
  reg = (char)temp;

  temp = strtol(argv[3], NULL, 16);
  data = (char)temp;
 }


 file = open("/dev/i2c-1", O_RDWR);

 if(file<0){
  printf("Error opening the i2c bus.");
  return(1);
 }

 if(ioctl(file, I2C_SLAVE, addr) < 0) {
  printf("Error changing i2c slave address.");
  return(1);
 }

 buf[0] = reg;
 buf[1] = data;
 if(write(file, buf, 2) != 2) {
  printf("Error writing to device.");
 }

}
