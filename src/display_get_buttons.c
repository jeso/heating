#include <linux/i2c-dev.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){

 int file;
 char buf[1];

 file = open("/dev/i2c-1", O_RDWR);

 if(file<0){
  //printf("Error opening the i2c bus.");
  return(1);
 }

 if(ioctl(file, I2C_SLAVE, 0x71) < 0) {
  //printf("Error changing i2c slave address.");
  return(1);
 }

 buf[0] = 0x04;
 if(write(file, buf, 1) != 1) {
  //printf("Error writing to device.");
  return(1);
 }

 if(read(file, buf, 1) != 1) {
  //printf("Error reading from device.");
  return(1);
 }

 if(buf[0]&0x01) printf("1 "); else printf("0 ");
 if(buf[0]&0x02) printf("1 "); else printf("0 ");
 if(buf[0]&0x04) printf("1 "); else printf("0 ");
 if(buf[0]&0x08) printf("1 "); else printf("0 ");
 if(buf[0]&0x10) printf("1 "); else printf("0 ");
 if(buf[0]&0x20) printf("1 "); else printf("0 ");
 if(buf[0]&0x40) printf("1 "); else printf("0 ");
 if(buf[0]&0x80) printf("1"); else printf("0");

}
