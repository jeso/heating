#include <linux/i2c-dev.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){

 int i, temp, len;
 int file, line;
 char buf[18];

 if(argc < 4){
   printf("Need line, position and data!\n");
   return(1);
 }

 if( strcmp(argv[1], "1" ) == 0){
   buf[0] = 0x08;
 }
 else if( strcmp(argv[1], "2" ) == 0){
   buf[0] = 0x18;
 }
 else{
   printf("Bad line.\n");
   return(1);
 }

 if( strcmp(argv[2], "0") == 0 )  { buf[0] += 0;  len = 16; }
 else if( strcmp(argv[2], "1") == 0 )  { buf[0] += 1;  len = 15; }
 else if( strcmp(argv[2], "2") == 0 )  { buf[0] += 2;  len = 14; }
 else if( strcmp(argv[2], "3") == 0 )  { buf[0] += 3;  len = 13; }
 else if( strcmp(argv[2], "4") == 0 )  { buf[0] += 4;  len = 12; }
 else if( strcmp(argv[2], "5") == 0 )  { buf[0] += 5;  len = 11; }
 else if( strcmp(argv[2], "6") == 0 )  { buf[0] += 6;  len = 10; }
 else if( strcmp(argv[2], "7") == 0 )  { buf[0] += 7;  len = 9;  }
 else if( strcmp(argv[2], "8") == 0 )  { buf[0] += 8;  len = 8;  }
 else if( strcmp(argv[2], "9") == 0 )  { buf[0] += 9;  len = 7;  }
 else if( strcmp(argv[2], "10") == 0 ) { buf[0] += 10; len = 6;  }
 else if( strcmp(argv[2], "11") == 0 ) { buf[0] += 11; len = 5;  }
 else if( strcmp(argv[2], "12") == 0 ) { buf[0] += 12; len = 4;  }
 else if( strcmp(argv[2], "13") == 0 ) { buf[0] += 13; len = 3;  }
 else if( strcmp(argv[2], "14") == 0 ) { buf[0] += 14; len = 2;  }
 else if( strcmp(argv[2], "15") == 0 ) { buf[0] += 15; len = 1;  }
 else{
   printf("Error, wrong character number");
   return(1);
 }

 strncpy(&buf[1], argv[3], len);

 file = open("/dev/i2c-1", O_RDWR);

 if(file<0){
  printf("Error opening the i2c bus.");
  return(1);
 }

 if(ioctl(file, I2C_SLAVE, 0x71) < 0) {
  printf("Error changing i2c slave address.");
  return(1);
 }

 if(write(file, buf, strlen(buf)) != strlen(buf)) {
  printf("Error writing to device.");
 }

}
