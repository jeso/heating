#include <linux/i2c-dev.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){

 int i;
 int file;
 char buf[128];
 char addr;
 memset(buf, 0, 128);

 if(argc < 2){
   printf("Need address!\n");
   return(1);
 }

 if(argc == 2){

  if( strcmp(argv[1], "0x51" ) == 0){
    addr = 0x51;
  }
  else if( strcmp(argv[1], "0x52" ) == 0){
    addr = 0x52;
  }
  else if( strcmp(argv[1], "0x61" ) == 0){
    addr = 0x61;
  }
  else if( strcmp(argv[1], "0x62" ) == 0){
    addr = 0x62;
  }
  else if( strcmp(argv[1], "0x71" ) == 0){
    addr = 0x71;
  }
  else if( strcmp(argv[1], "0x72" ) == 0){
    addr = 0x72;
  }
  else{
    printf("Unknown address\n");
    return(1);
  }

 }

 file = open("/dev/i2c-1", O_RDWR);

 if(file<0){
  printf("Error opening the i2c bus.\n");
  return(1);
 }

 if(ioctl(file, I2C_SLAVE, addr) < 0) {
  printf("Error changing i2c slave address.\n");
  return(1);
 }

 buf[0] = 0;
 if(write(file, buf, 1) != 1) {
  //printf("Error writing to device.");
 }

 if(read(file, buf, 128) != 128) {
  //printf("Error reading from device.");
  for(i=0; i<128; i++){
    printf("XX ");
  }
  return(1);
 }

 for(i=0; i<128; i++){
   printf("%X ", buf[i]);
 }

}
