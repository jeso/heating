#include <linux/i2c-dev.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){

 int i, file;
 char buf[1];
 int count = 0;

 file = open("/dev/i2c-1", O_RDWR);

 if(file<0){
  printf("Error opening the i2c bus.");
  return(1);
 }

 buf[0] = 0x03;
 ioctl(file, I2C_SLAVE, 0x51);
 if(write(file, buf, 1) != 1){}
 else if(read(file, buf, 1) != 1){}
 else if(buf[0] & 0x10)   		printf("ERR-0X51-WDT ");
 else if(buf[0] & 0x20) { 		printf("ERR-0X51-INP ");
   buf[0] = 0x05;
   write(file, buf, 1);
   read(file, buf, 1);
   if( buf[0] & 0x01 ) printf("51-LOW-0 ");
   if( buf[0] & 0x02 ) printf("51-LOW-1 ");
   if( buf[0] & 0x04 ) printf("51-LOW-2 ");
   if( buf[0] & 0x08 ) printf("51-LOW-3 ");
   if( buf[0] & 0x10 ) printf("51-LOW-4 ");
   if( buf[0] & 0x20 ) printf("51-LOW-5 ");
   if( buf[0] & 0x40 ) printf("51-LOW-6 ");
   if( buf[0] & 0x80 ) printf("51-LOW-7 ");

   buf[0] = 0x06;
   write(file, buf, 1);
   read(file, buf, 1);
   if( buf[0] & 0x01 ) printf("51-HI-0 ");
   if( buf[0] & 0x02 ) printf("51-HI-1 ");
   if( buf[0] & 0x04 ) printf("51-HI-2 ");
   if( buf[0] & 0x08 ) printf("51-HI-3 ");
   if( buf[0] & 0x10 ) printf("51-HI-4 ");
   if( buf[0] & 0x20 ) printf("51-HI-5 ");
   if( buf[0] & 0x40 ) printf("51-HI-6 ");
   if( buf[0] & 0x80 ) printf("51-HI-7 ");
 }

 buf[0] = 0x03;
 ioctl(file, I2C_SLAVE, 0x52);
 if(write(file, buf, 1) != 1){}
 else if(read(file, buf, 1) != 1){}
 else if(buf[0] & 0x10)  		printf("ERR-0X52-WDT ");
 else if(buf[0] & 0x20) {  		printf("ERR-0X52-INP ");
   buf[0] = 0x05;
   write(file, buf, 1);
   read(file, buf, 1);
   if( buf[0] & 0x01 ) printf("52-LOW-0 ");
   if( buf[0] & 0x02 ) printf("52-LOW-1 ");
   if( buf[0] & 0x04 ) printf("52-LOW-2 ");
   if( buf[0] & 0x08 ) printf("52-LOW-3 ");
   if( buf[0] & 0x10 ) printf("52-LOW-4 ");
   if( buf[0] & 0x20 ) printf("52-LOW-5 ");
   if( buf[0] & 0x40 ) printf("52-LOW-6 ");
   if( buf[0] & 0x80 ) printf("52-LOW-7 ");

   buf[0] = 0x06;
   write(file, buf, 1);
   read(file, buf, 1);
   if( buf[0] & 0x01 ) printf("52-HI-0 ");
   if( buf[0] & 0x02 ) printf("52-HI-1 ");
   if( buf[0] & 0x04 ) printf("52-HI-2 ");
   if( buf[0] & 0x08 ) printf("52-HI-3 ");
   if( buf[0] & 0x10 ) printf("52-HI-4 ");
   if( buf[0] & 0x20 ) printf("52-HI-5 ");
   if( buf[0] & 0x40 ) printf("52-HI-6 ");
   if( buf[0] & 0x80 ) printf("52-HI-7 ");
 }



 buf[0] = 0x03;
 ioctl(file, I2C_SLAVE, 0x61);
 if(write(file, buf, 1) != 1){}
 else if(read(file, buf, 1) != 1){}
 else if(buf[0] & 0x10)  		printf("ERR-0X61-WDT ");

 buf[0] = 0x03;
 ioctl(file, I2C_SLAVE, 0x71);
 if(write(file, buf, 1) != 1){}
 else if(read(file, buf, 1) != 1){}
 else if(buf[0] & 0x10)  		printf("ERR-0X71-WDT ");



}
