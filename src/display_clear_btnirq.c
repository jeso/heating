#include <linux/i2c-dev.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){

 int file;
 char buf[2];
 buf[0] = 0x05;

 if( argc > 1){
   if( strcmp(argv[1], "1")==0 ) buf[1] = 0x01;
   if( strcmp(argv[1], "2")==0 ) buf[1] = 0x02;
   if( strcmp(argv[1], "3")==0 ) buf[1] = 0x04;
   if( strcmp(argv[1], "4")==0 ) buf[1] = 0x08;
   if( strcmp(argv[1], "5")==0 ) buf[1] = 0x10;
   if( strcmp(argv[1], "6")==0 ) buf[1] = 0x20;
   if( strcmp(argv[1], "7")==0 ) buf[1] = 0x40;
   if( strcmp(argv[1], "8")==0 ) buf[1] = 0x80;
 }
 else
   buf[1]= 0xff;

 file = open("/dev/i2c-1", O_RDWR);

 if(file<0){
  printf("Error opening the i2c bus.");
  return(1);
 }

 if(ioctl(file, I2C_SLAVE, 0x71) < 0) {
  printf("Error changing i2c slave address.");
  return(1);
 }

 if(write(file, buf, 2) != 2) {
  printf("Error writing to device.");
 }

}
