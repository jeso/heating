#include <linux/i2c-dev.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){

 int file, i;
 char buf[8];

 file = open("/dev/i2c-1", O_RDWR);

 if(file<0){
  printf("Error opening the i2c bus.");
  return(1);
 }

 if(ioctl(file, I2C_SLAVE, 0x51) < 0) {
  printf("Error changing i2c slave address.");
  return(1);
 }

 buf[0] = 0x08;
 if(write(file, buf, 1) != 1) {
  printf("Error writing to device.");
 }

 if(read(file, buf, 8) != 8) {
  printf("Error reading from device.");
 }

 for(i=0; i<8; i++){
   printf("%d ", buf[i]);
 }


 if(ioctl(file, I2C_SLAVE, 0x52) < 0) {
  printf("Error changing i2c slave address.");
  return(1);
 }

 buf[0] = 0x08;
 if(write(file, buf, 1) != 1) {
  printf("Error writing to device.");
 }

 if(read(file, buf, 8) != 8) {
  printf("Error reading from device.");
 }

 for(i=0; i<8; i++){
   // Korekce kanalu
   if(i==4){
     printf("%d ", (buf[i]+5));
   }
   else{
     printf("%d ", buf[i]);
   }
 }

}
