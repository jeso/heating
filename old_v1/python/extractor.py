#!/usr/bin/python

# Following function gets i2cdump output file,
# and converts it to a raw data bytes array
# in the form:
# [DB0][DB1][DB2]...

import string

def i2c_extract_bytes( file ):
 f = open(file, 'r')
 data = []
 split = []
 out = []

 # Read lines from file to array
 for i in range(0,17):
  data.append(f.readline())
 f.close();

 # Split lines to words
 for i in range(0,17):
  split.append(string.split(data[i]));

 # Create output byte array
 for line in range(1, 9):
  for byte in range(1, 17):
   out.append(split[line][byte])

 # Return as array
 return out
