#!/usr/bin/python
import json

P = [0,0,0,0,0,0,0,0]
f = open('p_vars', 'r')
P[0]	= int(f.readline())	# Teplota vody pro podlahu
P[1]	= int(f.readline())	# Teplota vody pro radiator
P[2]	= int(f.readline())	# Teplota zpatecky pro radiator
P[3]	= int(f.readline())	# Teplota pro natapeni bojleru
P[4]	= int(f.readline())	# Teplota pro sepnuti obehu kotle
P[5]	= int(f.readline())	# Doba podavani
P[6]	= int(f.readline())	# Doba mezi podavanim
P[7]	= int(f.readline())	# Teplota na nadrzi pro zatopeni
f.close()

f = open('c_vars', 'r')
MODE = f.readline()
f.close()

Tk 	= sen1_mem[8];		# Vystup kotle
Ts	= sen1_mem[9];		# Teplota sneku
Tt	= sen1_mem[10];		# Teplota topeniste
Tn1	= sen1_mem[11];		# Teplota nadrze 1
Tn2	= sen1_mem[12];		# Teplota nadrze 2
Tn3	= sen1_mem[13];		# Teplota nadrze 3
Tp	= sen1_mem[14];		# Teplota podlahy
Tpz	= sen1_mem[15];		# Teplota podlahy zpatecka

Trz	= sen2_mem[8];		# Teplota radiatoru zpatecka
Tb	= sen2_mem[9];		# Teplota bojleru
Tv	= sen2_mem[10];		# Teplota venku

R[0]	= 0;	# Cerpadlo 1 podlaha  | Pomala zmena, hystereze -> sekundova PWM
R[1]	= 0;	# Cerpadlo 2 radiator | Pomala zmena, hystereze -> sekundova PWM
R[2]	= 0;	# Cerpadlo 3 kotel    | Bud pomala zmena nebo plnej knedlik podle programu
R[3]    = 0;	# Pohon snehu	      | Prikladani jednou za x minut -> minutova PWM
R[4]	= 0;	# Ventilator 1  \
R[5]	= 0;	# Ventilator 2	|->  Jeden ze tri na minutovou PWM
R[6]	= 0;	# Ventilator 3  /
R[7]	= 0;	# Spirala zatapeni    | Jednorazove na x sekund -> MKO

#################################################################

