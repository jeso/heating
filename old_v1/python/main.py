#!/usr/bin/python
from extractor import i2c_extract_bytes
import os

def include(filename):
 if os.path.exists(filename):
  execfile(filename)

sen1_mem = i2c_extract_bytes('data/sen1.dat')
sen2_mem = i2c_extract_bytes('data/sen2.dat')
rel1_mem = i2c_extract_bytes('data/rel1.dat')

sen1_alive = 0
sen2_alive = 0
rel1_alive = 0

if(sen1_mem[0] == "XX"):
 print("Sensor board 1 not responding!")
elif(sen1_mem[1] == "01"):
 print("Sensor board 1 found!")
 sen1_alive = 1
else:
 print("Sensor board 1 fucked up!")

if(sen2_mem[0] == "XX"):
 print("Sensor board 2 not responding!")
elif(sen2_mem[1] == "01"):
 print("Sensor board 2 found!")
 sen2_alive = 1
else:
 print("Sensor board 2 fucked up!")

if(rel1_mem[0] == "XX"):
 print("Relay board 1 not responding!")
elif(rel1_mem[1] == "02"):
 print("Relay board 1 found!")
 rel1_alive = 1
else:
 print("Relay board 1 fucked up!")


#if((sen1_alive==0)or(sen2_alive==0)or(rel1_alive==0)):
# # ALARM + HW_RST
# print("Lost at least one board. Resetting to fail-safe.")
# os.system("./main.sh -r")
# os.system("sendmail `cat mailing/mail_receiver` < mailing/mail_lost_board")
#else:
# print("Running logic.")
# include('logic.py')

print("Running logic.")
include('logic.py')

