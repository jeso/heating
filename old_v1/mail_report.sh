#!/bin/bash

# Sender, subject
cat ./mailing/mail_base > ./mailing/mail.txt

# Contents
echo " " >> ./mailing/mail.txt
echo Sensor board 1 data: >> ./mailing/mail.txt
echo " " >> ./mailing/mail.txt
cat ./data/sen1.dat >> ./mailing/mail.txt

echo " " >> ./mailing/mail.txt
echo Sensor board 2 data: >> ./mailing/mail.txt
echo " " >> ./mailing/mail.txt
cat ./data/sen2.dat >> ./mailing/mail.txt

echo " " >> ./mailing/mail.txt
echo Relay board 1 data: >> ./mailing/mail.txt
echo " " >> ./mailing/mail.txt
cat ./data/rel1.dat >> ./mailing/mail.txt

echo " " >> ./mailing/mail.txt
echo Bash log: >> ./mailing/mail.txt
echo " " >> ./mailing/mail.txt
cat ./log/bash_part.log >> ./mailing/mail.txt

echo " " >> ./mailing/mail.txt
echo Python log: >> ./mailing/mail.txt
echo " " >> ./mailing/mail.txt
cat ./log/python_part.log >> ./mailing/mail.txt

# Send email
sendmail dan.machaty@gmail.com < ./mailing/mail.txt

# Remove temporary mail text
rm ./mailing/mail.txt
