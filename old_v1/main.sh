#!/bin/bash

# Assure that board are powered up
echo 1 > /sys/class/gpio/export > /dev/null
echo out > /sys/class/gpio/gpio1_pg0/direction > /dev/null
echo 1 > /sys/class/gpio/gpio1_pg0/value > /dev/null

# Arguments ##################################################

# Help
if [ "$1" = "-h" ]; then
  echo "Heating regulation main program"
  echo "Usage: ./main.sh [-h|-r|-l|-f]"
  echo " -h   This help"
  echo " -r   Reconfigure boards"
  echo " -l   Show number of visible boards"
  echo " -f   Force boards restart and reconfigure"

# List boards
elif [ "$1" = "-l" ]; then
  echo -n "Visible boards count: "
  echo `i2cdetect -y 1 | grep -e 51 -e 52 -e 61 -e 62 | wc -l`

# Force reconfigure boards
elif [ "$1" = "-f" ]; then
 echo "Forcing reconfigure.."
 echo " - boards off"
 echo 0 > /sys/class/gpio/gpio1_pg0/value
 sleep 1
 echo " - boards on"
 echo 1 > /sys/class/gpio/gpio1_pg0/value
 sleep 1
 echo " - remove configured flag"
 rm configured
 echo " - call reconfigure"
 ./main.sh -r
 echo "Done"

elif [ "$1" = "-r" ]; then
 rm configured
 echo "Reconfiguring boards:"
 # Reset boards
 echo " - boards reset"
 i2cset -y 1 0x51 0x03 0x80
 i2cset -y 1 0x52 0x03 0x80
 i2cset -y 1 0x61 0x03 0x80
 # Allow inputs
 echo " - enable inputs"
 i2cset -y 1 0x51 0x04 0xFF
 i2cset -y 1 0x52 0x04 0xFF
 # Allow outputs
 echo " - enable outputs"
 i2cset -y 1 0x61 0x04 0xFF
 # Lock
 touch 'configured'
 #sendmail `cat mailing/mail_receiver` < ./mailing/mail_reconfigure
 #echo "Done, nofitication sent."
 echo -n `date +'[%Y-%m-%d %H:%M:%S]'` >> log/bash_part.log
 echo " Boards reconfigured." >> log/bash_part.log
  
else

 # If boards are not configured, do so
 if [ -f 'configured' ];
 then
   touch 'configured'
 else
   ./main.sh -r
 fi

 # Pull data from external boards
 echo -n "Loading the data from boards.."
 i2cdump -y 1 0x51 b > data/sen1.dat
 i2cdump -y 1 0x52 b > data/sen2.dat
 i2cdump -y 1 0x61 b > data/rel1.dat
 echo "done"

 # Run regulation sequence
 echo "Starting python regulation sequence.."
 python python/main.py
 echo "Finished!"

fi
