#!/bin/bash

# Set conf_running flag
touch conf_running

success=1

inputs_1=0x07
inputs_2=0xFF

echo "----------------------"
echo "Reconfiguration script"

# For log reference
date

echo -n	" Testing sound .......................	"
./shell_scripts/beep.sh

echo -n " Turning boards off (to be sure) .....	"
./shell_scripts/power_off.sh > /dev/null
echo "done"

echo -n " Reseting status files ................"
cat ./python/var_null > ./python/var_r1
cat ./python/var_null > ./python/var_r2
cat ./python/var_null > ./python/var_r3
cat ./python/var_null > ./python/var_r4
cat ./python/var_null > ./python/var_r5
cat ./python/var_null > ./python/var_r6
cat ./python/var_null > ./python/var_r7
cat ./python/var_null > ./python/var_r8
echo "done"

echo -n " Waiting 2s to power down ............	"
sleep 2
echo "done"

echo -n " Turning boards on ...................	"
./shell_scripts/power_on.sh > /dev/null
echo "done"

echo -n " Waiting 2s to power up ..............	"
sleep 2
echo "done"

detect=`./bin/detect_boards`
#ERR_MOD
if [ "$detect" = "S1_OK S2_OK R1_OK D1_OK " ] || [ "$detect" = "S1_OK S2_NOK R1_OK D1_OK " ]
then
  echo " Searching for boards .................	found"
else
  echo " Searching for boards ................. not found!"
  echo $detect
  echo " Exiting!"
  rm conf_running
  exit 1
fi

echo -n " Enabling inputs on sensor board 1 ...	"
./bin/i2c_write_quick 0x51 0x04 $inputs_1
echo "done"

echo -n " Enabling inputs on sensor board 2 ...	"
./bin/i2c_write_quick 0x52 0x04 $inputs_2
echo "done"

echo -n " Waiting for inputs to convert .......	"
sleep 5
echo "done"

echo -n " Checking inputs .....................	"

errors=`./bin/get_errors`
if [ "$errors" == "ERR-0X51-INP 51-HI-1" ]
then
  errors=""
fi
if [ "$errors" == "" ]
then
  echo "ok"
else
  echo "fail"
  success=0
fi



echo -n " Writing first display data ..........	"
if [ $success == 1 ]
then
  ./bin/display_write_line 1 0 Konfigurace
  ./bin/display_write_line 2 0 v_poradku
else
  ./bin/display_write_line 1 0 Chyba
  ./bin/display_write_line 2 0 konfigurace
fi
echo "done"

if [ $success == 1 ]
then
  touch configured
  echo "-------------------------------"
else
  echo "Errors during configuration, not enabling regulation."
  echo "Reported error:"
  echo $errors
fi

rm conf_running
