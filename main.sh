#!/bin/bash
#date +"%T" > start_time				# <Pepa_4> - pocitani casu start
#starttime=$(date +"%s")
#cat start_time >> run_time				# </Pepa_4>
# If configuration running, don't start again!
if [ -e "./conf_running" ]
then
  echo "Configuration already running!!"
  exit 1
fi

# If boards are not configured, configure them
if [ ! -e "./configured" ]
then
  echo "Boards not configured, will reconfigure."
  ./shell_scripts/configure.sh
else
  board_status=`./bin/detect_boards`
  if [ "$board_status" = "S1_OK S2_OK R1_OK D1_OK " ] || [ "$board_status" = "S1_OK S2_NOK R1_OK D1_OK " ]
  then
    errors=`./bin/get_errors`
    if [ "$errors" = "ERR-0X51-INP 51-HI-1 " ]		# <Pepa_1> - ignorovani statusu cidla TKK
    then
      errors=""
    fi							# </Pepa_1>
    if [ "$errors" = "" ]
    then
      ./bin/sensors_get_temps > ./data/temps
      ./bin/display_get_buttons > ./data/buttons
      ./python/main.py >> data/log.log 2>&1

      tail -n 60000 ./data/temps.log > ./data/temps_t.log
      echo "" >> ./data/temps_t.log
      cat ./data/temps >> ./data/temps_t.log
      rm ./data/temps.log
      mv ./data/temps_t.log ./data/temps.log
      tail -n 8650 ./data/temps.log > ./data/temps_chart.log
      rm ./data/temps_prew
      cp ./data/temps ./data/temps_prew
    else
      echo "Errors found!"
      echo "Error list:"
      echo $errors
      echo ""
      echo "Beeping an alert.."
      ./shell_scripts/beep.sh
      rm configured					# <Pepa_2> - pokud nastane chyba tak rekonfiguruj </Pepa_2>
    fi

  else
    ./shell_scripts/power_off.sh
    ./shell_scripts/beep.sh
    echo "Restarting boards due to error"
    ./shell_scripts/power_on.sh
    cat ./python/var_null > ./python/var_r1		# <Pepa_3> - restartujes-li boardy smaz stavy v souborech
    cat ./python/var_null > ./python/var_r2
    cat ./python/var_null > ./python/var_r3
    cat ./python/var_null > ./python/var_r4
    cat ./python/var_null > ./python/topim		# </Pepa_3>
    sleep 5
    rm configured
  fi
fi
#date +"%T" > finish_time				# <Pepa_5> - pocitani casu konec a zapis
#endtime=$(date +"%s")
#echo $((endtime - starttime)) >> run_time
#cat finish_time >> run_time				# </Pepa_5>
