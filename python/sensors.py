import os
import subprocess
import string

def temperatures_get():
  file = open("./data/temps", "r")
  temperatures = file.read()
  file.close()
  temperatures_ar = string.split(temperatures)
  file = open("./data/temps_prew", "r")				#<Pepa> predchozi status a zprumerovani
  temperatures = file.read()
  file.close()
  temperatures_prew = string.split(temperatures)
  for i in range(0,15):
    if( temperatures_ar[i] == "255" ):
      temperatures_ar[i]="101"
    temperatures_ar[i] = str(( int(temperatures_prew[i]) + int(temperatures_ar[i]) ) /2 )
  temperatures = string.join(temperatures_ar)
  file = open("./data/temps", "w")				#<Pepa> ulozeni
  file.write(temperatures)
  file.close()							#</Pepa>
  return temperatures_ar
