#!/usr/bin/python

import time
import os
import string
import subprocess
import pickle
import smtplib

import buttons
import sensors

# Read the variables				                moved to &variables& part
#file = open("./python/var_ok_mode_par", "r")
#ok_mode_par = pickle.load(file)
#file.close()

#file = open("./python/var_line_base_ptr", "r")
#line_base_ptr = pickle.load(file)
#file.close()

#file = open("./python/var_line_sub1_ptr", "r")
#line_sub1_ptr = pickle.load(file)
#file.close()

#file = open("./python/var_mode", "r")
#mode = pickle.load(file)
#file.close()

# Send Email program
def send_email(message):
 from email.mime.text import MIMEText
 if (message == 0):
  msg = MIMEText("... Vypinam kotel - natopeno ...")
 elif (message == 1):
  msg = MIMEText("... Zapinam kotel ...")
 elif (message == 2):
  msg = MIMEText("... Doslo Palivo!! ...")
 msg['subject'] = 'Kotel'
 msg['from'] = 'kotel@machaty.cz'
 msg['to'] = 'joey.m@seznam.cz'
 s = smtplib.SMTP('smtp.seznam.cz', 25)
 s.login("joey.m@seznam.cz",'Jo.800404')
 s.sendmail('joey.m@seznam.cz',['joey.m@seznam.cz'], msg.as_string())


# Read var from file program
def read_var_from_file(file):
 f = open(file)
 var = f.read()
 f.close()
 return var

# Load data
buttons_status_ar = buttons.buttons_get();
temperatures_ar = sensors.temperatures_get();

## Variables

ok_mode_par	= int(read_var_from_file('./python/var_ok_mode_par'))	# stav topeni
line_base_ptr	= int(read_var_from_file('./python/var_line_base_ptr'))	# stav topeni
line_sub1_ptr	= int(read_var_from_file('./python/var_line_sub1_ptr'))	# stav topeni
TOPIM		= int(read_var_from_file('./python/topim'))		# stav topeni
mode		= int(read_var_from_file('./python/var_mode'))		# mod topeni
POUT		= int(read_var_from_file('./python/var_pout'))		# parametr topeni ekvitermika
P0		= int(read_var_from_file('./python/var_p0'))		# zapni topne teleso (1 / 0)
P1		= int(read_var_from_file('./python/var_p1'))		# mintep pro beh cerpadla podlahy
P2		= int(read_var_from_file('./python/var_p2'))		# mintep pro beh cerpadla radiatoru
P3		= int(read_var_from_file('./python/var_p3'))		# tep zpatecky podlahy pro beh cerpadla podlahy	
P4		= int(read_var_from_file('./python/var_p4'))		# mintep v nadrzi pro zatop
P5		= int(read_var_from_file('./python/var_p5'))		# mintep kotle pro beh cerpadla kotle
P6		= int(read_var_from_file('./python/var_p6'))		# doba behu sneku
P7		= int(read_var_from_file('./python/var_p7'))		# doba opakovani behu sneku
P8		= int(read_var_from_file('./python/var_p8'))		# doba behu ventilatoru
P9		= int(read_var_from_file('./python/var_p9'))		# doba opakovani behu ventilatoru
P10		= int(read_var_from_file('./python/var_p10'))		# doba behu michadla
P11		= int(read_var_from_file('./python/var_p11'))		# doba opakovani behu michadla
R1		= int(read_var_from_file('./python/var_r1'))		# stav cerpadla podlahy
R2		= int(read_var_from_file('./python/var_r2'))		# stav cerpadla kotle
R3		= int(read_var_from_file('./python/var_r3'))		# stav napajeni Moeleru (ovladani kotle)
R4		= int(read_var_from_file('./python/var_r4'))		# stav topeni elektrickym telesem



# Setup static variables - jen nastaveni nuloveho radku displeje
if mode == 0:
  if( TOPIM == 1 ):
    line1 = "A T TKK"+temperatures_ar[1]+" TK"+temperatures_ar�r[12]
  elif( TOPIM == 2 ):
    line1 = "A D TKK"+temperatures_ar[1]+" TK"+temperatures_ar[12]
  elif( TOPIM == 0 ):
    line1 = "A N TKK"+temperatures_ar[1]+" TK"+temperatures_ar[12]
elif mode == 1:
  line1 = "ZAT TKK"+temperatures_ar[1]+" TK"+temperatures_ar[12]
elif mode == 2:
  line1 = "VYPNUTO"

#elif mode == 3:
#  line1 = "E "

lines_base = ["TK="+temperatures_ar[12]+" TPT="+temperatures_ar[10],
	      "TPZ="+temperatures_ar[8]+" TRZ="+temperatures_ar[9],
	      "TDP="+temperatures_ar[11]+" TTT="+temperatures_ar[0],
	      "TN1="+temperatures_ar[15]+" TN2="+temperatures_ar[14],
	      "TN3="+temperatures_ar[13]+" TKK="+temperatures_ar[1]]
line_base_ptr_max = len(lines_base)-1

lines_sub1 = ["P0="+str(P0),
	      "P1="+str(P1),
	      "P2="+str(P2),
	      "P3="+str(P3),
	      "P4="+str(P4),
	      "P5="+str(P5),
	      "P6="+str(P6),
	      "P7="+str(P7),
	      "P8="+str(P8),
	      "P9="+str(P9),
	      "P10="+str(P10),
	      "P11="+str(P11)]
line_sub1_ptr_max = len(lines_sub1)-1

# Buttons handling

# Btn Ter base (Termostat)
if( buttons_status_ar[6] == "1" ):	#sedme tlacitko vyhrazeno pro signal termostatu
  signal_term = 1
else:
  signal_term = 0

# Base screen
if( ok_mode_par == 0):			#zakladni menu (ukazuje teploty)
  # Btn down base
  if( buttons_status_ar[4] == "1" ):
    line_base_ptr = line_base_ptr + 1
    if(line_base_ptr > line_base_ptr_max):
      line_base_ptr = 0
  # Btn up base
  if( buttons_status_ar[0] == "1" ):
    line_base_ptr -= 1
    if(line_base_ptr < 0):
      line_base_ptr = line_base_ptr_max
  # Btn reset base
  if( buttons_status_ar[3] == "1" ):
    subprocess.call(["rm", "configured"])
    subprocess.call(["rm", "-rf", "./data/*"])
    exit(1)
  # Btn Ok base
  if( buttons_status_ar[1] == "1" ):
    ok_mode_par = 1
  # Btn Esc base (change mode)
  if( buttons_status_ar[5] == "1" ):
    if( (mode == 0) & (TOPIM == 2) ):
      mode = 1
      TOPIM = 1
    elif( (mode == 0) & (TOPIM != 2) ):
      mode = 2
    elif( mode == 1):
      mode = 2
    elif( mode == 2):
      mode = 1
      TOPIM = 1
# Parameter screen
elif( ok_mode_par == 1 ):		#prvni submenu (ukazuje parametry)
  # Btn down sub1
  if( buttons_status_ar[4] == "1" ):
    line_sub1_ptr = line_sub1_ptr + 1
    if(line_sub1_ptr > line_sub1_ptr_max):
      line_sub1_ptr = 0
  # Btn up sub1
  if( buttons_status_ar[0] == "1" ):
    line_sub1_ptr -= 1
    if(line_sub1_ptr < 0):
      line_sub1_ptr = line_sub1_ptr_max
  # Btn Esc sub1
  if( buttons_status_ar[5] == "1" ):
    ok_mode_par = 0
  # Btn Ok sub1
  if( buttons_status_ar[1] == "1" ):
    ok_mode_par = 2
# Set-parameter screen
elif( ok_mode_par == 2 ):	# Druhe submenu nastavuje parametr ze sub1
# Get parameter for edit
  if( line_sub1_ptr == 0 ):
    file_p = "./python/var_p0"
    p_p = P0
  elif( line_sub1_ptr == 1 ):
    file_p = "./python/var_p1"
    p_p = P1
  elif( line_sub1_ptr == 2 ):
    file_p = "./python/var_p2"
    p_p = P2
  elif( line_sub1_ptr == 3 ):
    file_p = "./python/var_p3"
    p_p = P3
  elif( line_sub1_ptr == 4 ):
    file_p = "./python/var_p4"
    p_p = P4
  elif( line_sub1_ptr == 5 ):
    file_p = "./python/var_p5"
    p_p = P5
  elif( line_sub1_ptr == 6 ):
    file_p = "./python/var_p6"
    p_p = P6
  elif( line_sub1_ptr == 7 ):
    file_p = "./python/var_p7"
    p_p = P7
  elif( line_sub1_ptr == 8 ):
    file_p = "./python/var_p8"
    p_p = P8
  elif( line_sub1_ptr == 9 ):
    file_p = "./python/var_p9"
    p_p = P9
  elif( line_sub1_ptr == 10 ):
    file_p = "./python/var_p10"
    p_p = P10
  elif( line_sub1_ptr == 11 ):
    file_p = "./python/var_p11"
    p_p = P11

  # Btn UpDown sub2 parameter P0
  if( (line_sub1_ptr == 0) & ( (buttons_status_ar[4] == "1") | (buttons_status_ar[1] == "1") ) ):
    file = open(file_p, "w")
    if( P0 == 1 ):
      file.write("0")
    elif( P0 == 0 ):
      file.write("1")
    file.close()

  # Btn Down sub2
  elif( (buttons_status_ar[4] == "1") & (line_sub1_ptr <> 0)):
    file = open(file_p, "w")
    file.write(str(p_p-1))
    file.close()

  # Btn Up sub2
  elif( (buttons_status_ar[0] == "1") & (line_sub1_ptr <> 0) ):
    file = open(file_p, "w")
    file.write(str(p_p+1))
    file.close()
  
  # Btn OK and Esc sub2
  elif( (buttons_status_ar[1] == "1") | (buttons_status_ar[5] == "1") ):
    ok_mode_par = 1


# Calculate status

## Temperatures
TTT	= int(temperatures_ar[0])	#teplota u topneho telesa
TKK	= int(temperatures_ar[1])	#teplota kominu za kotlem
TIN	= int(temperatures_ar[2])	#teplota interieru
#TOUT	= int(temperatures_ar[3])	#teplota exterieru
TPZ	= int(temperatures_ar[8])	#teplota podlaha zpatecka
TRZ	= int(temperatures_ar[9])	#teplota radiator zpatecka
TPT	= int(temperatures_ar[10])	#teplota pro topeni
TDP	= int(temperatures_ar[11])	#teplota do podlahy
TK	= int(temperatures_ar[12])	#teplota kotel
TN3	= int(temperatures_ar[13])	#teplota nadrz dolni
TN2	= int(temperatures_ar[14])	#teplota nadrz uprostred
TN1	= int(temperatures_ar[15])	#teplota nadrz horni

# Automatic change of mode from Z to A when TK is more then 60
if( (TK > (P5 + 3)) & (mode == 1) ):
  mode = 0
#elif(  ):
#  mode = 1

# Set parameter POUT on base of TOUT
TOUT = 1
if( TOUT == 0 ):
  POUT = 28
TCIL = 20
# Set parameter TCIL on base of time
if( (time.localtime().tm_hour >= 22) & (time.localtime().tm_hour < 6) ):
  TCIL = 18
elif( (time.localtime().tm_hour >= 6) & (time.localtime().tm_hour < 10) ):
  TCIL = 22
elif( (time.localtime().tm_hour >= 10) & (time.localtime().tm_hour < 18) ):
  TCIL = 20
elif( (time.localtime().tm_hour >= 18) & (time.localtime().tm_hour < 20) ):
  TCIL = 22
elif( (time.localtime().tm_hour >= 20) & (time.localtime().tm_hour < 22) ):
  TCIL = 21

# Change of parameter P3 on the base of TIN -- lepsi?
if( (TIN <= (TCIL - 1)) & (P3 != (POUT+1)) ):
  P3 = (POUT+1)
  file = open("./python/var_p3", "w")
  file.write(str(P3))
  file.close
elif( (TIN == TCIL) & (P3 != POUT) ):
  P3 = POUT
  file = open("./python/var_p3", "w")
  file.write(str(P3))
  file.close
elif( (TIN == (TCIL + 1)) & (P3 != (POUT-1)) ):
  P3 = (POUT-1)
  file = open("./python/var_p3", "w")
  file.write(str(P3))
  file.close
elif( (TIN == (TCIL + 2)) & (P3 != (POUT-2)) ):
  P3 = (POUT-2)
  file = open("./python/var_p3", "w")
  file.write(str(P3))
  file.close
elif( (TIN == (TCIL + 3)) & (P3 != (POUT-3)) ):
  P3 = (POUT-3)
  file = open("./python/var_p3", "w")
  file.write(str(P3))
  file.close
elif( (TIN >= (TCIL + 4)) & (P3 != (POUT-4)) ):
  P3 = (POUT-4)
  file = open("./python/var_p3", "w")
  file.write(str(P3))
  file.close

## Vystupy

### Rele 1 Cerpadlo - podlaha (zasuvka 1)
### Zapnout, pokud je teplota v nadrzi pro topeni vetsi nez P1 a o 2 vetsi nez teplota zpatecky
### a pokud je teplota zpatecky mensi nez nadrz-2st.
if ( (R1 == 0) & ( mode != 2) ):  
  if( (((TPT > (P1+1)) & (TPZ < (TPT - 2)) & (TPZ < (P3-1))) | (P0 == 1)) ):
    subprocess.call(["./bin/relays_set", "0", "0x01", "0x01", "0x00" ])
    file = open("./python/var_r1", "w")
    file.write("1")
    file.close()
elif ( R1 == 1 ):
#  if( mode == 0 ):
  if( ((TPT < (P1-1)) | (TPZ > (P3+1)) | (mode == 2)) & (P0 == 0) ): 
    subprocess.call(["./bin/relays_set", "0", "0x00", "0x00", "0x00" ])
    file = open("./python/var_r1", "w")
    file.write("0")
    file.close()


### Rele 2 Cerpadlo - kotel (zasuvka 2)
## pokud teplota z kotle (TK) >  P5 (60C), sepni cerpadlo kotle
if( (TK > (P5 + 3)) & (R2 == 0) ):
  subprocess.call(["./bin/relays_set", "1", "0x01", "0x01", "0x00" ])
  file = open("./python/var_r2", "w")
  file.write("1")
  file.close()
elif( (TK < (P5 - 2)) & (R2 == 1) ):
  subprocess.call(["./bin/relays_set", "1", "0x00", "0x00", "0x00" ])
  file = open("./python/var_r2", "w")
  file.write("0")
  file.close()

### Rele 3 Napajeni kotel (zasuvka 3) 
## napajeni Moelera
if( R3 == 0 ):
  if( mode == 0 ):
    if( (TK > 50) & (TN1 < P4) ): 
      subprocess.call(["./bin/relays_set", "2", "0x01", "0x01", "0x00" ])
      file = open("./python/var_r3", "w")
      file.write("1")
      file.close()
      send_email(1)
  elif( mode == 1 ):
    if( TN1 < P4 ):
      subprocess.call(["./bin/relays_set", "2", "0x01", "0x01", "0x00" ])
      file = open("./python/var_r3", "w")
      file.write("1")
      file.close()
elif( R3 == 1):
  if( mode == 0 ):
    if( TN1 > (P4 + 5) ):
      subprocess.call(["./bin/relays_set", "2", "0x09", str(hex(10)), str(hex(30)) ])
      file = open("./python/var_r3", "w")
      file.write("0")
      file.close()
      send_email(0)
    elif(TK < 50):
      subprocess.call(["./bin/relays_set", "2", "0x00", "0x00", "0x00" ])
      file = open("./python/var_r3", "w")
      file.write("0")
      file.close()
      TOPIM = 2
      send_email(2)
  elif( mode == 1 ):
    if( TN1 > (P4 + 5) ):
      subprocess.call(["./bin/relays_set", "2", "0x09", str(hex(5)), str(hex(30)) ])
      file = open("./python/var_r3", "w")
      file.write("0")
      file.close()
  elif( mode == 2 ):
      subprocess.call(["./bin/relays_set", "2", "0x00", "0x00", "0x00" ])
      file = open("./python/var_r3", "w")
      file.write("0")
      file.close()

### Rele 4 Topne teleso (prvni mala zasuvka)
## 
if( P0 == 0 ):
  if( (TPZ < (P3-1)) & ((mode == 0) | (mode == 1)) & (TPT < 30) ):
    file = open("./python/var_p0", "w")
    file.write("1")
    file.close()
  elif( R4 == 1 ):
    subprocess.call(["./bin/relays_set", "3", "0x00", "0x00", "0x00" ])
    file = open("./python/var_r4", "w")
    file.write("0")
    file.close()
    file = open("./cas_patrona", "a")
    file.write("F"+str(time.time())+"\n")
    file.close()
if( P0 == 1 ):
  if( (TK > 60) | ((TPZ > P3) & ((mode == 0) | (mode == 1))) | (TTT > 30) ):
    file = open("./python/var_p0", "w")
    file.write("0")
    file.close()
  elif( R4 == 0 ):
    subprocess.call(["./bin/relays_set", "3", "0x01", "0x01", "0x00" ])
    file = open("./python/var_r4", "w")
    file.write("1")
    file.close()
    file = open("./cas_patrona", "a")
    file.write("S"+str(time.time())+"\n")
    file.close()

# Topeni kotlem

### Rele 5 snek
## Pust snek na P6 jednou za P7
### Rele 6 ventilator
## Pust ventilator na P8 jednou za P9
### Rele 7 Michadlo
## Pust michadlo na P10 jednou za P11

if( TOPIM == 0 ):
  if( mode == 0 ):
    if( (TK > 50) & (TN1 < P4) ):
      TOPIM = 1
      subprocess.call(["./bin/relays_set", "4", "0x05", str(hex(P6)), str(hex(P7)) ])
      subprocess.call(["./bin/relays_set", "5", "0x05", str(hex(P8)), str(hex(P9)) ])
      #  if( P10 < 0 ):
      #    subprocess.call(["./bin/relays_set", "6", "0x05", hex(P10), hex(P11) ])
  elif( mode == 1):
    if( TN1 < P4 ):
      TOPIM = 1
      subprocess.call(["./bin/relays_set", "4", "0x05", str(hex(P6)), str(hex(P7)) ])
      subprocess.call(["./bin/relays_set", "5", "0x05", str(hex(P8)), str(hex(P9)) ])
elif( TOPIM == 1):
  if( mode == 0 ):
    if( TK < 40 ):
      ERROR = 1
      subprocess.call(["./bin/relays_set", "4", "0x00", "0x00", "0x00" ])
      subprocess.call(["./bin/relays_set", "5", "0x00", "0x00", "0x00" ])
      ## !!! DOSLO PALIVO NEBO JINY PROBLEM !!!
    if( TN1 > (P4+5) ):
      TOPIM = 0
      subprocess.call(["./bin/relays_set", "4", "0x06", "0x03", "0x09" ])
      subprocess.call(["./bin/relays_set", "5", "0x06", "0x03", "0x09" ])
      #  subprocess.call(["./bin/relays_set", "6", "0x00", "0x00", "0x00" ])
  elif( mode == 1 ):
    if( TN1 > (P4+5) ):
      TOPIM = 0
      subprocess.call(["./bin/relays_set", "4", "0x06", "0x03", "0x09" ])
      subprocess.call(["./bin/relays_set", "5", "0x06", "0x03", "0x09" ])
  elif( mode == 2 ):
    subprocess.call(["./bin/relays_set", "4", "0x00", "0x00", "0x00" ])
    subprocess.call(["./bin/relays_set", "5", "0x00", "0x00", "0x00" ])

### Rele 8 Cerpadlo - radiator
### Zapnout, pokud je teplota v nadrzi 1 vetsi nez P2
### a pokud je teplota TRZ < P3 s hysterezi
#if( TN1 > P2 ):
#  if( TRZ < (P3-2) ):
#    subprocess.call(["./bin/relays_set", "7", "0x01", "0x01", "0x00" ])
#  if( TRZ > (P3+2) ):
#    subprocess.call(["./bin/relays_set", "7", "0x00", "0x00", "0x00" ])


# Set relays

# Display output
subprocess.call(["./bin/display_clear_line", "1"]);
subprocess.call(["./bin/display_clear_line", "2"]);
subprocess.call(["./bin/display_write_line", "1", "0", line1 ]);
file = open("./display1", "w")
file.write(line1)
file.close()
if( ok_mode_par == 0):
  subprocess.call(["./bin/display_write_line", "2", "0", lines_base[line_base_ptr] ]);
  file = open("./display2", "w")
  file.write(lines_base[line_base_ptr])
  file.close()
elif( ok_mode_par == 1 ):
  subprocess.call(["./bin/display_write_line", "2", "0", lines_sub1[line_sub1_ptr] ]);
  file = open("./display2", "w")
  file.write(lines_sub1[line_sub1_ptr])
  file.close()
elif( ok_mode_par == 2 ):
  subprocess.call(["./bin/display_write_line", "2", "0", "Nastav - "+lines_sub1[line_sub1_ptr] ]);
  file = open("./display2", "w")
  file.write("Nastav - "+lines_sub1[line_sub1_ptr])
  file.close()

# Save variables
file = open("./python/var_pout", "w")
file.write(str(POUT))
file.close()

file = open("./python/var_ok_mode_par", "w")
pickle.dump(ok_mode_par, file)
file.close()

file = open("./python/var_mode", "w")
file.write(str(mode))
file.close()

file = open("./python/topim", "w")
file.write(str(TOPIM))
file.close()

if( ok_mode_par == 0):
  file = open("./python/var_line_base_ptr", "w+")
  pickle.dump(line_base_ptr, file)
  file.close()
else:
  file = open("./python/var_line_sub1_ptr", "w+")
  pickle.dump(line_sub1_ptr, file)
  file.close()
