#!/usr/bin/python

import os
import string
import subprocess
import pickle

import buttons
import sensors

# Read the variables
file = open("./python/var_ok_mode_par", "r")
ok_mode_par = pickle.load(file)
file.close()

file = open("./python/var_line_base_ptr", "r")
line_base_ptr = pickle.load(file)
file.close()

file = open("./python/var_line_sub1_ptr", "r")
line_sub1_ptr = pickle.load(file)
file.close()

file = open("./python/var_mode", "r")
mode = pickle.load(file)
file.close()

# Read var from file program
def read_var_from_file(file):
 f = open(file)
 var = f.read()
 f.close()
 return var

# Load data
buttons_status_ar = buttons.buttons_get();
temperatures_ar = sensors.temperatures_get();

## Variables
P1	= int(read_var_from_file('./python/var_p1'))
P2	= int(read_var_from_file('./python/var_p2'))
P3	= int(read_var_from_file('./python/var_p3'))
P4	= int(read_var_from_file('./python/var_p4'))
P5	= int(read_var_from_file('./python/var_p5'))
P6	= int(read_var_from_file('./python/var_p6'))
P7	= int(read_var_from_file('./python/var_p7'))
P8	= int(read_var_from_file('./python/var_p8'))

# Setup static variables
line1 = "Rezim:_auto_____"
lines_base = [#"T1="+temperatures_ar[0]+"_T2="+temperatures_ar[1],
  	      #"T3="+temperatures_ar[2]+"_T4="+temperatures_ar[3],
	      #"T5="+temperatures_ar[4]+"_T6="+temperatures_ar[5],
	      #"T7="+temperatures_ar[6]+"_T8="+temperatures_ar[7],
	      "TK="+temperatures_ar[12]+"_TP="+temperatures_ar[10],
	      "TPZ="+temperatures_ar[8]+"_TRZ="+temperatures_ar[9],
	      "TN1="+temperatures_ar[15]+"_TN2="+temperatures_ar[14],
	      "TN3="+temperatures_ar[13]+"_T12="+temperatures_ar[11]]
line_base_ptr_max = len(lines_base)-1
lines_sub1 = ["P1="+read_var_from_file('./python/var_p1'),
	      "P2="+read_var_from_file('./python/var_p2'),
	      "P3="+read_var_from_file('./python/var_p3'),
	      "P4="+read_var_from_file('./python/var_p4'),
	      "P5="+read_var_from_file('./python/var_p5'),
	      "P6="+read_var_from_file('./python/var_p6'),
	      "P7="+read_var_from_file('./python/var_p7'),
	      "P8="+read_var_from_file('./python/var_p8')]
line_sub1_ptr_max = len(lines_sub1)-1

# Buttons handling
# Base screen
if( ok_mode_par == 0):
  # Btn down base
  if( buttons_status_ar[4] == "1" ):
    line_base_ptr = line_base_ptr + 1
    if(line_base_ptr > line_base_ptr_max):
      line_base_ptr = 0
  # Btn up base
  if( buttons_status_ar[0] == "1" ):
    line_base_ptr -= 1
    if(line_base_ptr < 0):
      line_base_ptr = line_base_ptr_max
  # Btn reset base
  if( buttons_status_ar[3] == "1" ):
    subprocess.call(["rm", "configured"])
    subprocess.call(["rm", "-rf", "./data/*"])
    exit(1)
  # Btn Ok base
  if( buttons_status_ar[7] == "1" ):
    ok_mode_par = 1
else:
  # Btn down sub1
  if( buttons_status_ar[4] == "1" ):
    line_sub1_ptr = line_sub1_ptr + 1
    if(line_sub1_ptr > line_sub1_ptr_max):
      line_sub1_ptr = 0
  # Btn up sub1
  if( buttons_status_ar[0] == "1" ):
    line_sub1_ptr -= 1
    if(line_sub1_ptr < 0):
      line_sub1_ptr = line_sub1_ptr_max
  # Btn Ok sub1
  if( buttons_status_ar[7] == "1" ):
    ok_mode_par = 0
    line_sub1_ptr = 1
    
   
# Calculate status

## Temperatures
TK	= int(temperatures_ar[12])
#TS	= int(temperatures_ar[1])
#TT	= int(temperatures_ar[2])
#TN1	= int(temperatures_ar[3])
#TN2	= int(temperatures_ar[4])
#TN3	= int(temperatures_ar[5])
TP	= int(temperatures_ar[10])
TPZ	= int(temperatures_ar[8])
#TRZ	= int(temperatures_ar[8])
#TB	= int(temperatures_ar[9])
#TV	= int(temperatures_ar[10])

## Vystupy

### Cerpadlo1 - podlaha
### Zapnout, pokud je teplota v nadrzi 1 vetsi nez P1
### a pokud je teplota zpatecky mensi nez nadrz-2st.
if( (TP > P1) & (TPZ < (TP - 2) ) ):
  subprocess.call(["./bin/relays_set", "0", "0x01", "0x01", "0x00" ])
else:
  subprocess.call(["./bin/relays_set", "0", "0x00", "0x00", "0x00" ])

### Cerpadlo 2 - kotel
## pokud teplota z kotle (TK) >  P5 (60C), sepni cerpadlo kotle
if( TK > (P5 + 3) ):
  subprocess.call(["./bin/relays_set", "1", "0x01", "0x01", "0x00" ])
elif( TK < (P5 - 2) ):
  subprocess.call(["./bin/relays_set", "1", "0x00", "0x00", "0x00" ])


### Cerpadlo2 - radiator
### Zapnout, pokud je teplota v nadrzi 1 vetsi nez P2
### a pokud je teplota TRZ < P3 s hysterezi
#if( TN1 > P2 ):
#  if( TRZ < (P3-2) ):
#    subprocess.call(["./bin/relays_set", "1", "0x01", "0x01", "0x00" ])
#  if( TRZ > (P3+2) ):
#    subprocess.call(["./bin/relays_set", "1", "0x00", "0x00", "0x00" ])







# Set relays

# Display output
subprocess.call(["./bin/display_clear_line", "1"]);
subprocess.call(["./bin/display_clear_line", "2"]);
subprocess.call(["./bin/display_write_line", "1", "0", line1 ]);
if( ok_mode_par == 0):
  subprocess.call(["./bin/display_write_line", "2", "0", lines_base[line_base_ptr] ]);
else:
  subprocess.call(["./bin/display_write_line", "2", "0", lines_sub1[line_sub1_ptr] ]);

# Save variables
file = open("./python/var_ok_mode_par", "w+")
pickle.dump(ok_mode_par, file)
file.close()

if( ok_mode_par == 0):
  file = open("./python/var_line_base_ptr", "w+")
  pickle.dump(line_base_ptr, file)
  file.close()
else:
  file = open("./python/var_line_sub1_ptr", "w+")
  pickle.dump(line_sub1_ptr, file)
  file.close()
