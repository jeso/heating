P-Parametry regulace.<br><br>

<?php

$P0 = file_get_contents('../python/var_p0');
$P1 = file_get_contents('../python/var_p1');
$P2 = file_get_contents('../python/var_p2');
$POUT = file_get_contents('../python/var_pout');
$P3 = file_get_contents('../python/var_p3');
$P4 = file_get_contents('../python/var_p4');
$P5 = file_get_contents('../python/var_p5');
$P6 = file_get_contents('../python/var_p6');
$P7 = file_get_contents('../python/var_p7');
$P8 = file_get_contents('../python/var_p8');
$P9 = file_get_contents('../python/var_p9');
$P10 = file_get_contents('../python/var_p10');
$P11 = file_get_contents('../python/var_p11');

?>

 <form action="var_set.php" method="GET">

   Manualni topeni patronou:<br>
   P1=&nbsp<input type="text" name="P0" value="<?php print($P0); ?>" size=4>
   &nbsp&#176C<br><br>

   Minimalni teplota nadrze pro beh cerpadla podlahy:<br>
   P1=&nbsp<input type="text" name="P1" value="<?php print($P1); ?>" size=4>
   &nbsp&#176C<br><br>

   Minimalni teplota nadrze pro beh cerpadla radiatoru:<br>
   P2=&nbsp<input type="text" name="P2" value="<?php print($P2); ?>" size=4>
   &nbsp&#176C<br><br>

   Teplota zpatecky z podlahy pro beh cerpadla podlahy:<br>
   Pout=&nbsp<input type="text" name="POUT" value="<?php print($POUT); ?>" size=4>
   &nbsp&#176C&nbsp = > Vypocteno P3=<?php print($P3); ?><br><br>

   Minimalni teplota v nadrzi pro zatopeni<br>
   P4=&nbsp<input type="text" name="P4" value="<?php print($P4); ?>" size=4>
   &nbsp&#176C<br><br>

   Minimalni teplota kotle pro beh cerpadla kotle<br>
   P5=&nbsp<input type="text" name="P5" value="<?php print($P5); ?>" size=4>
   &nbsp&#176C<br><br>

   Doba, po kterou ma bezet pohon sneku<br>
   P6=&nbsp<input type="text" name="P6" value="<?php print($P6); ?>" size=4>
   Sekund<br><br>

   Doba mezi pridavanim snekem<br>
   P7=&nbsp<input type="text" name="P7" value="<?php print($P7); ?>" size=4>
   Sekund<br><br>

   Doba, po kterou ma bezet pohon ventilatoru<br>
   P8=&nbsp<input type="text" name="P8" value="<?php print($P8); ?>" size=4>
   Sekund<br><br>

   Doba mezi spustenim ventilatoru<br>
   P9=&nbsp<input type="text" name="P9" value="<?php print($P9); ?>" size=4>
   Sekund<br><br>

   Doba, po kterou ma bezet pohon michadla<br>
   P10=&nbsp<input type="text" name="P10" value="<?php print($P10); ?>" size=4>
   Sekund<br><br>

   Doba mezi spustenim michadla<br>
   P11=&nbsp<input type="text" name="P11" value="<?php print($P11); ?>" size=4>
   Sekund<br><br>

  <input type="submit" value="Nastavit">
 </form>
