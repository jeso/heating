<?php
$url1=$_SERVER['REQUEST_URI'];
header("Refresh: 30;URL=$url1");

$art = file('../data/temps_chart.log');
$artlen = sizeof($art);

?>

<html>
  <head>
    <script type="text/javascript"
          src="https://www.google.com/jsapi?autoload={
            'modules':[{
              'name':'visualization',
              'version':'1',
              'packages':['corechart']
            }]
          }"></script>

    <script type="text/javascript">
      google.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Index', /*'T0:TTT',*/ 'T1:TKK', 'T8:TPZ', /*'T9:TRZ',*/ 'T10:TPT', 'T11:TDP', 'T12:TK'/*, 'T13:TND', 'T14:TNP'*/, 'T15:TNH'],
		<?php
		for($j=0; $j<$artlen; $j++){
		 if(($j % 10) == 0){
			print("[");
			$temp = explode(" ",$art[$j]);
			print($j);
			print(", ");
			for($i=0; $i<=15; $i++){
			 if(/*$i==0 ||*/ $i==1 || $i==8 || $i==10 || $i==11 || $i==12 /*|| $i==13 || $i==14*/ || $i==15){
				print($temp[$i]);
				print(", ");
			 }
			}
			if($j<>($artlen)) {
				print("],");
			}
			else {
				print("]");
			}
		 }
		}
		?>
        ]);

        var options = {
          title: 'Grafy teplot',
          curveType: 'function',
          legend: { position: 'bottom' },
	  'width': 950,
	  backgroundColor: '#EAB035',
	  'explorer': {},
	  'smoothline':true
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="curve_chart" style="width: 800px; height: 500px"></div>
    <!--    Pocet zaznamu: <?php print($artlen); ?></br></br>  -->
  </body>
</html>
