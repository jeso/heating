<head>
 <link rel="stylesheet" type="text/css" href="style.css">
</head>

<div id="around">

<div id="header">
 <h1>Regulace</h1>
</div>

<div id="nav">
 <a href="index.php?pg=state">Stav systemu</a><br>
 <a href="index.php?pg=charts">Grafy teplot</a><br>
 <a href="index.php?pg=varset">Nastaveni parametru</a><br>
 <a href="index.php?pg=log">Vypis skriptu</a><br>
 <a href="index.php?pg=pymain">Regulacni skript python</a><br>
 <a href="index.php?pg=syslog">Vypis systemu</a><br>
 <a href="index.php?pg=update">Update regulace</a><br>
</div>

<div id="main">
 <?php if($_GET["pg"] == "") header("Location: index.php?pg=state"); ?>
 <?php if($_GET["pg"] == "state") include('pg_state.php'); ?>
 <?php if($_GET["pg"] == "varset") include('pg_varset.php'); ?>
 <?php if($_GET["pg"] == "log") include('pg_log.php'); ?>
 <?php if($_GET["pg"] == "pymain") include('pg_pymain.php'); ?>
 <?php if($_GET["pg"] == "syslog") include('pg_syslog.php'); ?>
 <?php if($_GET["pg"] == "update") include('pg_update.php'); ?>
 <?php if($_GET["pg"] == "charts") include('pg_charts.php'); ?>
</div>

<div id="footer">
Daniel Machaty 2015
</div>

</div>
