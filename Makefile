all: i2c global display sensors relays

i2c: 	 i2c_write_quick i2c_dump_quick
global:  detect_boards get_errors
display: display_write_line display_clear_line display_get_buttons display_clear_btnirq
sensors: sensors_get_temps
relays:  relays_set

clean:
	rm -f ./bin/*
	echo Clean done

i2c_write_quick: ./src/i2c_write_quick.c
	gcc ./src/i2c_write_quick.c -o ./bin/i2c_write_quick

i2c_dump_quick: ./src/i2c_dump_quick.c
	gcc ./src/i2c_dump_quick.c -o ./bin/i2c_dump_quick

display_write_line: ./src/display_write_line.c
	gcc ./src/display_write_line.c -o ./bin/display_write_line

display_clear_line: ./src/display_clear_line.c
	gcc ./src/display_clear_line.c -o ./bin/display_clear_line

display_get_buttons: ./src/display_get_buttons.c
	gcc ./src/display_get_buttons.c -o ./bin/display_get_buttons

display_clear_btnirq: ./src/display_clear_btnirq.c
	gcc ./src/display_clear_btnirq.c -o ./bin/display_clear_btnirq

detect_boards: ./src/detect_boards.c
	gcc ./src/detect_boards.c -o ./bin/detect_boards

sensors_get_temps: ./src/sensors_get_temps.c
	gcc ./src/sensors_get_temps.c -o ./bin/sensors_get_temps

get_errors: ./src/get_errors.c
	gcc ./src/get_errors.c -o ./bin/get_errors

relays_set: ./src/relays_set.c
	gcc ./src/relays_set.c -o ./bin/relays_set
