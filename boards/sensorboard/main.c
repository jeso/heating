#include <msp430.h> 

/**************************************************************************************************
 * Definice
 *************************************************************************************************/
#define I2C_ADDR			0x52
#define LED_RED_PORT		P2OUT
#define LED_RED_PIN			0x01
#define LED_BLUE_PORT		P2OUT
#define LED_BLUE_PIN		0x02
#define PSU_SW_EN_N_PORT	P3OUT
#define PSU_SW_EN_N_PIN		0x01

#define CALIBRATION_VCC_CORRECTION_P		12		// Mereni: { V[C31](3.307) / 5 * 1023 - DisplayedValue }
#define	CALIBRATION_VCC_CORRECTION_N		0		// Mereni: Pokud by kompenzace byla zaporna
#define CALIBRATION_REF_VOLTAGE_MV_X10		964		// Mereni: TP12
#define CALIBRATION_REF_SUPPLY_MV			3307	// Mereni: C31
#define CALIBRATION_GAIN_X100				6635	// Mereni: 100 * V[TP17] / (V[TP11]-V[TP12])
#define CALIBRATION_INPUT_CORRECTION_ROOM	3		// Mereni: { V[TP17] / 2.5 * 1023 - DisplayedValue }
#define CALIBRATION_INPUT_CORRECTION_100D	15		// Mereni: { V[TP17] / 2.5 * 1023 - DisplayedValue }
#define MUX_WAITTIME_MS						50		//	Pocet ms, po ktere je treba cekat na prepnuti kanalu
#define	INPUT_LIMIT_TOP						1023	//	Mereni cidla je platne pouze pokud je mensi nez toto cislo
#define INPUT_LIMIT_BOTTOM					8		//	Mereni cidla je platne pouze pokud je vetsi nez toto cislo


#define MEM_MAGIC				0x55
#define MEM_DEV_TYPE_SENSORS	0x01
#define MEM_STATUS_INIT_OK		0x01
#define	MEM_STATUS_CYCLE_OK		0x02
#define MEM_STATUS_WDT_ERR		0x10
#define	MEM_STATUS_INPUT_ERR	0x20
#define MEM_STATUS_RESET		0x80


typedef struct {
	/* 0x00 */	unsigned char	magic;		//	Musi byt 0x55 - detekce zarizeni

	/* 0x01 */	unsigned char	dev_type;	//	Typ desky
											//	0x01 = Sensor x 8
											//	0x02 = Rele x 8
											// 	0x03 = Tlacitka
											//	0x04 = Komunikace

	/* 0x02 */	unsigned char	watchdog;	//	Pokud dojde do nuly, spusti alarm

	/* 0x03	*/	unsigned char	status;		//	Status register
											// 	bit0 = Probehla inicializace pole
											// 	bit1 = Probehla alespon 1 smycka
											// 	bit2 = ..rezerva..
											// 	bit3 = ..rezerva..
											// 	bit4 = Chyba watchdogu
											//	bit5 = Chyba cidel
											//	bit6 = ..rezerva..
											//	bit7 = Vyvolat reset

	/* 0x04	*/	unsigned char	input_en;	//	Povoleni jednotlivych cidel a alarmu

	/* 0x05 */	unsigned char	err_low;	//	Zkrat na cidle (vstup podteceny), cidla po bitech
	/* 0x06 */	unsigned char	err_high;	//	Odpojene cidlo (vstup preteceny), cidla po bitech

	/* 0x07 */	unsigned char	reserved1;	//	..rezerva.. (kvuli zarovnani bytu)

	/* 0x08 */	unsigned char	temp0;		//	Teploty ve �C
	/* 0x09 */	unsigned char	temp1;
	/* 0x0A */	unsigned char	temp2;
	/* 0x0B */	unsigned char	temp3;
	/* 0x0C */	unsigned char	temp4;
	/* 0x0D */	unsigned char	temp5;
	/* 0x0E */	unsigned char	temp6;
	/* 0x0F */	unsigned char	temp7;

	/* 0x10 */	unsigned int	rawtemp0;	//	Bitova hodnota jednotlivych vstupu, 2 byty
	/* 0x12 */	unsigned int	rawtemp1;
	/* 0x14 */	unsigned int	rawtemp2;
	/* 0x16 */	unsigned int	rawtemp3;
	/* 0x18 */	unsigned int	rawtemp4;
	/* 0x1A */	unsigned int	rawtemp5;
	/* 0x1C */	unsigned int	rawtemp6;
	/* 0x1E */	unsigned int	rawtemp7;

	/* 0x20 */	unsigned int	rawinttemp;	//	Bitova hodnota vstupu teplotniho cidla, 2 byty
	/* 0x22 */	unsigned int	rawintvcca;	//	Bitova hodnota vstupu z delice napajeni, 2 byty
} t_MEM;


/**************************************************************************************************
 * Globalni promenne
 *************************************************************************************************/
t_MEM _MEM;						//	Auto-init to 0
unsigned char*	_i2c_txbuf;		//	TX Buffer start pointer
unsigned char*	_i2c_rxbuf;		//	RX Buffer start pointer
unsigned char	_i2c_bufpos;	//	I2C Data pointer, auto-init to 0
unsigned char	_i2c_txbuflen;	//	TX Buffer length
unsigned char	_i2c_rxbuflen;	//	RX Buffer length
unsigned long int tim_var_1;	//	Auto-decrementing variable 1
unsigned long int tim_var_2;	//	Auto-decrementing variable 1
unsigned long int _wdt_var;		//	Watchdog varible (ms->s)

/**************************************************************************************************
 * Prototypy funkci
 *************************************************************************************************/
void init_memory();
void init_ports();
void init_clocks();
void init_adc();
void init_i2c();
void init_ms_timer();
void adc_convert(unsigned int channel, unsigned int * dest_addr);
void led_red_blink_fast(unsigned char count);
void led_red_blink_slow(unsigned char count);
void sleep_ms(unsigned int msec);
void adc_scan();
void adc_calc_temp();

/**************************************************************************************************
 * Main
 *************************************************************************************************/
int main(void) {
	WDTCTL = WDTPW | WDTHOLD;	//	Zastavit watchdog
	_wdt_var = 1000;

	P2DIR |= 0x03;		//	Vystup pro LED
	P2OUT = 0x00;		//	Zapnout obe LED

	init_memory();		//	Inicializace hodnoty registru
	init_ports();		//	Inicializace portu
	init_clocks();		//	Inicializace hodin
	init_adc();			//	Inicializace A/D prevodniku
	init_i2c();			//	Inicializace I2C periferie
	init_ms_timer();	//	Inicializace milisekundoveho casovace

	led_red_blink_slow(3);	//	3x blinkout cervenou LED

	while(1){

		tim_var_2 = 1000;

		if(_MEM.status & MEM_STATUS_RESET)	WDTCTL = 0;
		if((_MEM.status & MEM_STATUS_WDT_ERR) || (_MEM.status & MEM_STATUS_INPUT_ERR))	LED_RED_PORT &= ~LED_RED_PIN;
		else	LED_RED_PORT |= LED_RED_PIN;


		adc_scan();

		adc_calc_temp();

		LED_BLUE_PORT &= ~LED_BLUE_PIN;
		sleep_ms(50);
		LED_BLUE_PORT |= LED_BLUE_PIN;
		sleep_ms(50);
		LED_BLUE_PORT &= ~LED_BLUE_PIN;
		sleep_ms(50);
		LED_BLUE_PORT |= LED_BLUE_PIN;
		sleep_ms(350);

		while(tim_var_2 > 0){
			__no_operation();
		}

		_MEM.watchdog = 0x14;
	}

}

/**************************************************************************************************
 * Function definitions
 *************************************************************************************************/

void init_memory()
{
	_MEM.magic		|=	MEM_MAGIC;				// Magicke cislo :-)
	_MEM.dev_type 	|=	MEM_DEV_TYPE_SENSORS;	// Dev type sensory
	_MEM.watchdog	|=	0x14;					// Watchdog na maximum
	_MEM.status		|=	MEM_STATUS_INIT_OK;		// Pole inicializovane
	_MEM.input_en	 = 	0x00;					// Zakaz vsechny vstupy
	_MEM.err_low	 =	0x00;					// Snuluj chyby
	_MEM.err_high	 = 	0x00;					// Snuluj chyby
	_MEM.reserved1	 =	0x00;					// Rezerva

	_MEM.temp0		|=	0xFF;	// Fail value
	_MEM.temp1		|=	0xFF;	// Fail value
	_MEM.temp2		|=	0xFF;	// Fail value
	_MEM.temp3		|=	0xFF;	// Fail value
	_MEM.temp4		|=	0xFF;	// Fail value
	_MEM.temp5		|=	0xFF;	// Fail value
	_MEM.temp6		|=	0xFF;	// Fail value
	_MEM.temp7		|=	0xFF;	// Fail value

	_i2c_txbuf = &_MEM.magic;
	_i2c_rxbuf = &_MEM.magic;
	_i2c_bufpos = 0;
	_i2c_txbuflen = 36;
	_i2c_rxbuflen = 36;
}

void init_ports()
{

	/*
	 * Pin#	Pin name		SEL	DIR	OUT	REN
	 * -------------------------------------
	 * 1.0	INPUT_SEL_A		0	1	0	0
	 * 1.1	INPUT_SEL_B		0	1	0	0
	 * 1.2	INPUT_SEL_C		0	1	0	0
	 * 1.3	NC				0	0	1	1
	 * 1.4	JTAG			0	0	0	0
	 * 1.5	JTAG			0	0	0	0
	 * 1.6	JTAG			0	0	0	0
	 * 1.7	JTAG			0	0	0	0
	 * SUMMARY				x00	x03	x08	x08
	 *
	 * 2.0	LED_RED			0	1	1	0
	 * 2.1	LED_BLUE		0	1	1	0
	 * 2.2	OC#_V_3V3_AN_SW	0	0	0	0
	 * 2.3	CPU_TP2			0	1	0	0
	 * 2.4	CPU_TP1			0	1	0	0
	 * 2.5	NC				0	0	1	1
	 * 2.6	XIN				1	0	0	0
	 * 2.7	XOUT			1	0	0	0
	 * SUMMARY				xC0	x1B	x23	x20
	 *
	 * 3.0	#EN_V_3V3_AN_SW	0	1	1	0
	 * 3.1	I2C_SDA			1	0	0	0
	 * 3.2	I2C_SCL			1	0	0	0
	 * 3.3	NC				0	0	1	1
	 * 3.4	NC				0	0	1	1
	 * 3.5	NC				0	0	1	1
	 * 3.6	NC				0	0	1	1
	 * 3.7	NC				0	0	1	1
	 * SUMMARY				x06	x01	xF9	xF8
	 *
	 * 4.0	NC				0	0	1	1
	 * 4.1	NC				0	0	1	1
	 * 4.2	NC				0	0	1	1
	 * 4.3	INPUT_DIFF		0	0	0	0 * ADC10AE1.3
	 * 4.4	NC				0	0	0	0
	 * 4.5	NC				0	0	0	0
	 * 4.6	NC				0	0	0	0
	 * 4.7	NC				0	0	0	0
	 * SUMMARY				x00	x00	x07	x07
	 */

	P1SEL = 0x00;
	P1DIR = 0x07;
	P1OUT = 0x08;
	P1REN = 0x08;

	P2SEL = 0xC0;
	P2DIR = 0x1B;
	P2OUT = 0x23;
	P2REN = 0x20;

	P3SEL = 0x06;
	P3DIR = 0x01;
	P3OUT = 0xF9;
	P3REN = 0xF8;

	P4SEL = 0x00;
	P4DIR = 0x00;
	P4OUT = 0x07;
	P4REN = 0x07;
}

void init_clocks(){

	unsigned int i;

	__bic_SR_register(OSCOFF);	// Start oscillator

	BCSCTL1 |= XTS;				// High-freq. mode
	BCSCTL3 |= LFXT1S1;			// When in HF mode, then 3-16MHz oscillator

	while(1){							// Loop until done
		LED_RED_PORT &= ~LED_RED_PIN;	// Signal error state, until osc ok
		IFG1 &= ~OFIFG;					// Clear OFIFG
		for(i=0xFF; i>0; i--);			// Delay
		if(!(IFG1 & OFIFG))
			break;				// If OFIFG is clear (no osc. fault), quit
	}

	LED_RED_PORT |= LED_RED_PIN;	// Error state off

	BCSCTL1 = XT2OFF + XTS + DIVA_3 + RSEL0;	// XT2 Off, HF mode, ACLK = 1MHz
	BCSCTL2 = SELM_3 + DIVM_0 + SELS + DIVS_0;	// MCLK->LFXT1, MCLK=8MHz, SMCLK->LFXT1, SMCLK=8MHz
}

void init_adc(){
	unsigned int i;
	ADC10AE1 |= 0x10;		// Set input 12 as Analog Input

	// 2.5V reference, ref. on, ADC10 On, longest S&H time (512us), reference from VRef+
	ADC10CTL0 |= REF2_5V + REFON + ADC10ON + ADC10SHT_0 + SREF_1;
	for(i=0xFF; i>0; i--);	// Wait for reference setup
}

void adc_convert(unsigned int channel, unsigned int * dest_addr){
	ADC10CTL0 &= ~ENC;						//	Disable conversion (to allow channel change)
	ADC10CTL1 = (channel * 0x1000) + SHS_0 + ADC10DIV_0 + ADC10SSEL_1;	// Set correct channel
	ADC10CTL0 |= ENC;						// Enable conversion

	ADC10CTL0 &= ~ADC10IFG;
	ADC10MEM = 0x0000;

	ADC10CTL0 |= ADC10SC;					// Start conv. up
	ADC10CTL0 &= ~ADC10SC;					// Start conv. down
	while(!(ADC10CTL0 & ADC10IFG)){
		__no_operation();					// Wait for conversion finish
	}
	ADC10CTL0 &= ~ENC;						// Disable conversion
	*dest_addr = ADC10MEM;	// Save result
}

void init_i2c(){
	__bic_SR_register(GIE);			// Disable interrupts
	UCB0CTL1 |= UCSWRST;			// Reset the I2C Controller
	UCB0CTL0 = UCMODE_3 + UCSYNC;	// 7bit, single-master, slave, I2C, sync.
	UCB0CTL1 &= ~UCSWRST;			// Release I2C controller from reset
	UCB0I2COA = I2C_ADDR;			// Device address
	IE2 |= UCB0TXIE;
	IE2 |= UCB0RXIE;				// Enable RX/TX interrupts
	__bis_SR_register(GIE);			// Enable interrupts
}

void init_ms_timer(){
	TACTL 	|= 	TACLR;			// Stop and clear the timer
	TACCTL0 |= 	CCIE;			// Enable CCIFG IRQ
	TACTL 	= 	TASSEL_1 + ID_0 + MC_1;
	TACCR0 	= 	0x3E8;			// 1:1000 divider
}

// Zablika rychle (bez delaye)
void led_red_blink_fast(unsigned char count){
	unsigned char i;

	if(count == 0){
		while(1){
			LED_RED_PORT &= ~LED_RED_PIN;
			LED_RED_PORT |=  LED_RED_PIN;
		}
	}

	for(i=count; i>0; i--){
		LED_RED_PORT &= ~LED_RED_PIN;
		LED_RED_PORT |=  LED_RED_PIN;
	}
}

// Zablika pomalu (delaye)
void led_red_blink_slow(unsigned char count){
	unsigned char i;
	if(count == 0){
		while(1){
			LED_RED_PORT &= ~LED_RED_PIN;
			sleep_ms(100);
			LED_RED_PORT |=  LED_RED_PIN;
			sleep_ms(100);
		}
	}

	for(i=count; i>0; i--){
		LED_RED_PORT &= ~LED_RED_PIN;
		sleep_ms(100);
		LED_RED_PORT |=  LED_RED_PIN;
		sleep_ms(100);
	}

	tim_var_1 = 200;
	while(tim_var_1);
}

//	Vycka uvedeny pocet milisekund
void sleep_ms(unsigned int msec){
	tim_var_1 = msec;
	while(tim_var_1);
}

//	Prevede zadane kanaly (nekalibrovane)
void adc_scan(){
	adc_convert(10, &_MEM.rawinttemp);		//	Prevod teplotniho cidla
	adc_convert(11, &_MEM.rawintvcca);		//	Prevod napajeciho napeti


	/*
	 * !!!!!!!!!!!!!!!!!! PREMAPOVANO NA KONEKTORY ZLEVA DOPRAVA !!!!!!!!!!!!!!!!!!!!!!!!!
	 */
	//	Prevod jednotlivych kanalu, pokud jsou povolene

	PSU_SW_EN_N_PORT &= ~PSU_SW_EN_N_PIN;	// Pustit napajeni do pullupu
	sleep_ms(30);							// Pockat na ustaleni (mereno cca 10ms)

	if(_MEM.input_en & 0x01)	{	P1OUT = 0x03;	sleep_ms(MUX_WAITTIME_MS);	adc_convert(12, &_MEM.rawtemp0);	}
	if(_MEM.input_en & 0x02)	{	P1OUT = 0x00;	sleep_ms(MUX_WAITTIME_MS);	adc_convert(12, &_MEM.rawtemp1);	}
	if(_MEM.input_en & 0x04)	{	P1OUT = 0x01;	sleep_ms(MUX_WAITTIME_MS);	adc_convert(12, &_MEM.rawtemp2);	}
	if(_MEM.input_en & 0x08)	{	P1OUT = 0x02;	sleep_ms(MUX_WAITTIME_MS);	adc_convert(12, &_MEM.rawtemp3);	}
	if(_MEM.input_en & 0x10)	{	P1OUT = 0x05;	sleep_ms(MUX_WAITTIME_MS);	adc_convert(12, &_MEM.rawtemp4);	}
	if(_MEM.input_en & 0x20)	{	P1OUT = 0x07;	sleep_ms(MUX_WAITTIME_MS);	adc_convert(12, &_MEM.rawtemp5);	}
	if(_MEM.input_en & 0x40)	{	P1OUT = 0x04;	sleep_ms(MUX_WAITTIME_MS);	adc_convert(12, &_MEM.rawtemp6);	}
	if(_MEM.input_en & 0x80)	{	P1OUT = 0x06;	sleep_ms(MUX_WAITTIME_MS);	adc_convert(12, &_MEM.rawtemp7);	}

	PSU_SW_EN_N_PORT |= PSU_SW_EN_N_PIN;	// Vypnout napajeni
}

//	Prevod hodnoty na stupne (kalibrovane)
void adc_calc_temp(){
	int i;

	unsigned long int	vcc_mv 		=	(unsigned long int)_MEM.rawintvcca;			// Inicializace promenne
						vcc_mv		=	vcc_mv + CALIBRATION_VCC_CORRECTION_P;		// |- Kompenzace chyby delice v MSP430 +
						vcc_mv		=	vcc_mv - CALIBRATION_VCC_CORRECTION_N;		// |- Kompenzace chyby delice v MSP430 -
						vcc_mv		=	vcc_mv * 5000;								// |- * 2 (delic) * 2.5 (reference) * 1000 (posun radove carky na cele mV)
						vcc_mv		= 	vcc_mv / 1023;								// |- Vysledna hodnota mV

	unsigned long int	vref_mv_x10	=	vcc_mv * CALIBRATION_REF_VOLTAGE_MV_X10;	// Trojclenka pro ziskani hodnoty reference pri soucasnem stavu napajeni
						vref_mv_x10	=	vref_mv_x10 / CALIBRATION_REF_SUPPLY_MV;	// |- Vysledek v mV*10 (96.4mV = 964)

	unsigned long int 	vin_mv 			=	0;
	unsigned long int 	vmux_mv_x10 	=	0;
	unsigned long int 	resistance_x10 	=	0;
	unsigned long int 	temperature		=	0;
	unsigned int		* rawtemp_base	=	&_MEM.rawtemp0;	//	Pointer na registr rawteplot 0
	unsigned char 		* temp_base		=	&_MEM.temp0;	//	Pointer na registr teplot 0

	for(i=0; i<8; i++){					//	Projdi vsechny kanaly

		if(_MEM.input_en & (1 << i)){	//	Pokud je kanal povoleny

			vin_mv	=	(unsigned long int)(*(rawtemp_base+i));

			if(vin_mv < INPUT_LIMIT_TOP){			//	Pokud nema pretecenou hodnotu (horni saturace)
				if(vin_mv > INPUT_LIMIT_BOTTOM){	//	Pokud nema podtecenou hodnotu (dolni saturace)

					unsigned long int	compensation_slope = CALIBRATION_INPUT_CORRECTION_100D * 1000;	// Smernice kalibracni primky (zmerena)
					compensation_slope = compensation_slope / 1023;

					unsigned long int	compensation = compensation_slope * vin_mv;	// Kompenzacni clen
					compensation = compensation / 1000;

					vin_mv	=	vin_mv + compensation;							// Kompenzace aditivni

					vin_mv	=	vin_mv * 2500;
					vin_mv	=	vin_mv / 1023;									// Vysledek v mV (napr. 625)

					vmux_mv_x10	=	vin_mv * 1000;								// Presun do x10 + Presun *100 kvuli nasledujicimu vypoctu
					vmux_mv_x10	=	vmux_mv_x10 / CALIBRATION_GAIN_X100;
					vmux_mv_x10	=	vmux_mv_x10 + vref_mv_x10;					// Vysledek v mV*10 (napr. 1051 pro 105.1mV)

					unsigned long int vcc_vmux_diff_mv_x10 = (10*vcc_mv - vmux_mv_x10);	// (napr. 32020)

					resistance_x10	=	vmux_mv_x10 * 10 * 3300;
					resistance_x10	=	resistance_x10 / (vcc_vmux_diff_mv_x10);	//	(napr. 1082)

					temperature	=	2597 * resistance_x10;
					temperature = 	temperature - 2595500;
					temperature =	temperature / 10000;

					*(temp_base+i) = (unsigned char)(temperature);

				}
				else{
					*(temp_base+i) 	= 0x00;						// Snuluj vystup (podteceno)
					_MEM.err_low 	|= (1 << i);				// Ohlas chybu podteceni
					_MEM.status		|= MEM_STATUS_INPUT_ERR;	// Ohlas chybu cidla
				}
			}
			else{
				*(temp_base+i) = 0xFF;						// Saturuj vystup (preteceno)
				_MEM.err_high 	|= (1 << i);				// Ohlas chybu preteceni
				_MEM.status		|= MEM_STATUS_INPUT_ERR;	// Ohlas chybu cidla
			}

		}
		else *(temp_base+i) = 0xFF;	// Pokud neni kanal povoleny tak napln saturacni hodnotou

	}	// Konec FOR
}	// Konec MAIN


/**************************************************************************************************
 * Obsluha interruptu
 *************************************************************************************************/
#pragma vector=USCIAB0TX_VECTOR		// I2C Data service routine (status je v USCIAB0RX_VECTOR)
__interrupt void USCIAB0TX_ISR(void){

	if(UCB0STAT & UCSTTIFG){
		UCB0STAT &= ~UCSTTIFG;
		if(IFG2 & UCB0RXIFG){
			_i2c_bufpos = UCB0RXBUF;
		}
	}

	if(IFG2 & UCB0TXIFG){
		if(_i2c_bufpos < _i2c_txbuflen){
			UCB0TXBUF = *(_i2c_txbuf + _i2c_bufpos);
			_i2c_bufpos++;
		}
		else{
			UCB0TXBUF = 0x00;
		}
	}

	if(IFG2 & UCB0RXIFG){
		*(_i2c_rxbuf + _i2c_bufpos) = UCB0RXBUF;
		_i2c_bufpos++;
	}
}

#pragma vector=TIMERA0_VECTOR				// Obsluha TIMERA0 (ms timer), watchdog
__interrupt void TIMERA0_VECTOR_ISR(void){
	if(tim_var_1 != 0) tim_var_1--;
	if(tim_var_2 != 0) tim_var_2--;


	if(!(_MEM.status & MEM_STATUS_WDT_ERR)){

		if((_wdt_var<=1000)&&(_wdt_var>0))	{

			_wdt_var--;

			if(_wdt_var == 0){

				_wdt_var = 1000;

				_MEM.watchdog--;

				if(_MEM.watchdog == 0){
					_MEM.status |= MEM_STATUS_WDT_ERR;
				}
			}

		}
	}
}
