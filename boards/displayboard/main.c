#include <msp430.h> 

/**************************************************************************************************
 * Definice
 *************************************************************************************************/
#define I2C_ADDR			0x71
#define LED_RED_PORT		P2OUT
#define LED_RED_PIN			0x02
#define LED_BLUE_PORT		P2OUT
#define LED_BLUE_PIN		0x01

#define LCD_PORT	P1OUT
#define LCD_CTL		P3OUT
#define LCD_CTL_E	0x80
#define LCD_CTL_RW	0x40
#define LCD_CTL_RS	0x20

#define BTN_PORT	P4IN

#define MEM_MAGIC				0x55
#define MEM_DEV_TYPE_DISPLAY	0x03
#define MEM_STATUS_INIT_OK		0x01
#define	MEM_STATUS_CYCLE_OK		0x02
#define MEM_STATUS_WDT_ERR		0x10
#define MEM_STATUS_RESET		0x80


typedef struct {
	/* 0x00 */	unsigned char	magic;			//	Musi byt 0x55 - detekce zarizeni

	/* 0x01 */	unsigned char	dev_type;		//	Typ desky
												//	0x01 = Sensor x 8
												//	0x02 = Rele x 8
												// 	0x03 = Tlacitka
												//	0x04 = Komunikace

	/* 0x02 */	unsigned char	watchdog;		//	Pokud dojde do nuly, spusti alarm

	/* 0x03	*/	unsigned char	status;			//	Status register
												// 	bit0 = Probehla inicializace pole
												// 	bit1 = Probehla alespon 1 smycka
												// 	bit2 = ..rezerva..
												// 	bit3 = ..rezerva..
												// 	bit4 = Chyba watchdogu
												//	bit5 = ..rezerva..
												//	bit6 = ..rezerva..
												//	bit7 = Vyvolat reset

	/* 0x04	*/	unsigned char	button_pressed;	//	Pokud bylo tlacitko stisknuto, tady se to objevi
	/* 0x05 */	unsigned char	button_rst;		//	Reset predchoziho registru

	/* 0x06 */	unsigned char	reserved1;		//	Rezerva (zarovnani 1)
	/* 0x07 */	unsigned char	reserved2;		//	Rezerva (zarovnani 2)

	/* 0x08 */	char	disp_line0[16];	//	Prvni radka displeje
	/* 0x18 */	char	disp_line1[16];	//	Druha radka displeje

} t_MEM;


/**************************************************************************************************
 * Globalni promenne
 *************************************************************************************************/
t_MEM _MEM;						//	Auto-init to 0
unsigned char*	_i2c_txbuf;		//	TX Buffer start pointer
unsigned char*	_i2c_rxbuf;		//	RX Buffer start pointer
unsigned char	_i2c_bufpos;	//	I2C Data pointer, auto-init to 0
const unsigned char	_i2c_txbuflen = 39;	//	TX Buffer length
const unsigned char	_i2c_rxbuflen = 39;	//	RX Buffer length
unsigned long int tim_ms_var;	//	Auto-decrementing variable 1
unsigned long int tim_main_var;	//	Hlavni promenna citace (sekundovy cyklus)
unsigned long int _wdt_var;		//	Watchdog varible (ms->s)


/**************************************************************************************************
 * Prototypy funkci
 *************************************************************************************************/
void init_memory();
void init_ports();
void init_clocks();
void init_i2c();
void init_ms_timer();
void led_red_blink_fast(unsigned char count);
void led_red_blink_slow(unsigned char count);
void sleep_ms(unsigned int msec);
void init_display();
void lcd_wr_cmd(int command);
void lcd_set(int pin);
void lcd_clr(int pin);
void lcd_wr_line(int line, char * characters);
void lcd_wr_data(char character);

/**************************************************************************************************
 * Main
 *************************************************************************************************/
int main(void) {
	int cycle_counter = 0;
	unsigned char temp;

	WDTCTL = WDTPW | WDTHOLD;	//	Zastavit watchdog
	_wdt_var = 1000;

	P2DIR |= LED_RED_PIN + LED_BLUE_PIN;		//	Vystup pro LED
	P2OUT &= ~(LED_RED_PIN + LED_BLUE_PIN);		//	Zapnout obe LED

	init_memory();		//	Inicializace hodnoty registru
	init_ports();		//	Inicializace portu
	init_clocks();		//	Inicializace hodin
	init_i2c();			//	Inicializace I2C periferie
	init_ms_timer();	//	Inicializace milisekundoveho casovace

	sleep_ms(2000);	// Pockat na displej

	init_display();		//	Inicializace displeje

	while(1){

		// Hlavni programova smycka na 0.1s
		tim_main_var = 100;

		if(_MEM.status & MEM_STATUS_RESET) WDTCTL = 0;

		// Tohle je tady proto, aby se displej neprekresloval moc casto (blika)
		if(cycle_counter == 10){
			lcd_wr_cmd(0x01);
			lcd_wr_line(0, _MEM.disp_line0);
			lcd_wr_line(1, _MEM.disp_line1);
			cycle_counter = 0;
		}

		_MEM.button_pressed &= ~_MEM.button_rst;	// Vynuluj flagy, pokud byly videt
		_MEM.button_rst = 0;						// Auto-reset
		temp = BTN_PORT;
		temp = ~temp;
		_MEM.button_pressed |= temp;			// Sesbirej nove flagy (paaduu do nuly - inverze)

		cycle_counter++;

		LED_BLUE_PORT &= ~LED_BLUE_PIN;
		sleep_ms(50);
		LED_BLUE_PORT |= LED_BLUE_PIN;

		while(tim_main_var);

	}

}

/**************************************************************************************************
 * Function definitions
 *************************************************************************************************/

void init_memory()
{
	int i;

	_MEM.magic		|=	MEM_MAGIC;				// Magicke cislo :-)
	_MEM.dev_type 	|=	MEM_DEV_TYPE_DISPLAY;	// Dev type sensory
	_MEM.watchdog	|=	0x14;					// Watchdog na maximum
	_MEM.status		|=	MEM_STATUS_INIT_OK;		// Pole inicializovane

	for(i=0; i<16; i++){
		_MEM.disp_line0[i] = 0x20;
		_MEM.disp_line1[i] = 0x20;
	}

	_i2c_txbuf = &_MEM.magic;
	_i2c_rxbuf = &_MEM.magic;
	_i2c_bufpos = 0;
}

void init_ports()
{

	/*
	 * Pin#	Pin name		SEL	DIR	OUT	REN
	 * -------------------------------------
	 * 1.0	LCD0			0	1	0	0
	 * 1.1	LCD1			0	1	0	0
	 * 1.2	LCD2			0	1	0	0
	 * 1.3	LCD3			0	1	0	0
	 * 1.4	LCD4			0	1	0	0
	 * 1.5	LCD5			0	1	0	0
	 * 1.6	LCD6			0	1	0	0
	 * 1.7	LCD7			0	1	0	0
	 * SUMMARY				x00	xFF	x00	x00
	 *
	 * 2.0	LED_RED			0	1	1	0
	 * 2.1	LED_BLUE		0	1	1	0
	 * 2.2	NC				0	0	1	1
	 * 2.3	NC				0	0	1	1
	 * 2.4	NC				0	0	1	1
	 * 2.5	NC				0	0	1	1
	 * 2.6	XIN				1	0	0	0
	 * 2.7	XOUT			1	0	0	0
	 * SUMMARY				xC0	x03	x3F	x3C
	 *
	 * 3.0	NC				0	0	1	1
	 * 3.1	I2C_SDA			1	0	0	0
	 * 3.2	I2C_SCL			1	0	0	0
	 * 3.3	NC				0	0	1	1
	 * 3.4	NC				0	0	1	1
	 * 3.5	LCD_RS			0	1	0	0
	 * 3.6	LCD_RW			0	1	0	0
	 * 3.7	LCD_E			0	1	0	0
	 * SUMMARY				x06	xE0	x19	x19
	 *
	 * 4.0	BTN0			0	0	0	0
	 * 4.1	BTN1			0	0	0	0
	 * 4.2	BTN2			0	0	0	0
	 * 4.3	BTN3			0	0	0	0
	 * 4.4	BTN4			0	0	0	0
	 * 4.5	BTN5			0	0	0	0
	 * 4.6	BTN6			0	0	0	0
	 * 4.7	BTN7			0	0	0	0
	 * SUMMARY				x00	x00	x00	x00
	 */

	P1SEL = 0x00;
	P1DIR = 0xFF;
	P1OUT = 0x00;
	P1REN = 0x00;

	P2SEL = 0xC0;
	P2DIR = 0x03;
	P2OUT = 0x3F;
	P2REN = 0x3C;

	P3SEL = 0x06;
	P3DIR = 0xE0;
	P3OUT = 0x19;
	P3REN = 0x19;

	P4SEL = 0x00;
	P4DIR = 0x00;
	P4OUT = 0xFF;
	P4REN = 0xFF;
}

void init_clocks(){

	unsigned int i;

	__bic_SR_register(OSCOFF);	// Start oscillator

	BCSCTL1 |= XTS;				// High-freq. mode
	BCSCTL3 |= LFXT1S1;			// When in HF mode, then 3-16MHz oscillator

	while(1){							// Loop until done
		LED_RED_PORT &= ~LED_RED_PIN;	// Signal error state, until osc ok
		IFG1 &= ~OFIFG;					// Clear OFIFG
		for(i=0xFF; i>0; i--);			// Delay
		if(!(IFG1 & OFIFG))
			break;				// If OFIFG is clear (no osc. fault), quit
	}

	LED_RED_PORT |= LED_RED_PIN;	// Error state off

	BCSCTL1 = XT2OFF + XTS + DIVA_3 + RSEL0;	// XT2 Off, HF mode, ACLK = 1MHz
	BCSCTL2 = SELM_3 + DIVM_0 + SELS + DIVS_0;	// MCLK->LFXT1, MCLK=8MHz, SMCLK->LFXT1, SMCLK=8MHz
}

void init_i2c(){
	__bic_SR_register(GIE);			// Disable interrupts
	UCB0CTL1 |= UCSWRST;			// Reset the I2C Controller
	UCB0CTL0 = UCMODE_3 + UCSYNC;	// 7bit, single-master, slave, I2C, sync.
	UCB0CTL1 &= ~UCSWRST;			// Release I2C controller from reset
	UCB0I2COA = I2C_ADDR;			// Device address
	IE2 |= UCB0TXIE;
	IE2 |= UCB0RXIE;				// Enable RX/TX interrupts
	__bis_SR_register(GIE);			// Enable interrupts
}

void init_ms_timer(){
	TACTL 	|= 	TACLR;			// Stop and clear the timer
	TACCTL0 |= 	CCIE;			// Enable CCIFG IRQ
	TACTL 	= 	TASSEL_1 + ID_0 + MC_1;
	TACCR0 	= 	0x3E8;			// 1:1000 divider
}

// Zablika rychle (bez delaye)
void led_red_blink_fast(unsigned char count){
	unsigned char i;

	if(count == 0){
		while(1){
			LED_RED_PORT &= ~LED_RED_PIN;
			LED_RED_PORT |=  LED_RED_PIN;
		}
	}

	for(i=count; i>0; i--){
		LED_RED_PORT &= ~LED_RED_PIN;
		LED_RED_PORT |=  LED_RED_PIN;
	}
}

// Zablika pomalu (delaye)
void led_red_blink_slow(unsigned char count){
	unsigned char i;
	if(count == 0){
		while(1){
			LED_RED_PORT &= ~LED_RED_PIN;
			sleep_ms(100);
			LED_RED_PORT |=  LED_RED_PIN;
			sleep_ms(100);
		}
	}

	for(i=count; i>0; i--){
		LED_RED_PORT &= ~LED_RED_PIN;
		sleep_ms(100);
		LED_RED_PORT |=  LED_RED_PIN;
		sleep_ms(100);
	}

	sleep_ms(200);
}

//	Vycka uvedeny pocet milisekund
void sleep_ms(unsigned int msec){
	tim_ms_var = msec;
	while(tim_ms_var);
}

void init_display(){
	lcd_wr_cmd(0x38);
	lcd_wr_cmd(0x0F);
	lcd_wr_cmd(0x01);
	lcd_wr_line(0, "Test displeje...");
	sleep_ms(500);

	LED_BLUE_PORT &= ~LED_BLUE_PIN;
	//LED_RED_PORT &= ~LED_RED_PIN;
	lcd_wr_cmd(0x01);
	lcd_wr_line(0, "\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff");
	lcd_wr_line(1, "\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff");
	sleep_ms(500);
	LED_BLUE_PORT |= LED_BLUE_PIN;
	//LED_RED_PORT |= LED_RED_PIN;

	lcd_wr_cmd(0x01);

	lcd_wr_line(0, _MEM.disp_line0);
	lcd_wr_line(1, _MEM.disp_line1);
}

void lcd_wr_cmd(int command){
	LCD_CTL &= ~LCD_CTL_RS;
	LCD_CTL &= ~LCD_CTL_RW;
	LCD_PORT = command;		// Valid data
	LCD_CTL |= LCD_CTL_E;
	LCD_CTL &= ~LCD_CTL_E;
	sleep_ms(2);			// Tmax = 1.49ms
}

void lcd_wr_data(char character){
	LCD_CTL |= LCD_CTL_RS;
	LCD_CTL &= ~LCD_CTL_RW;
	LCD_PORT = character;		// Valid data
	LCD_CTL |= LCD_CTL_E;
	LCD_CTL &= ~LCD_CTL_E;
	sleep_ms(2);			// Tmax = 1.49ms
}

void lcd_wr_line(int line, char * characters){
	int i;

	lcd_wr_cmd(0x80 + line*0x40);

	for(i=0; i<16; i++){
		lcd_wr_data(*(characters+i));
	}

}

void lcd_set(int pin){
	LCD_CTL |= pin;
}

void lcd_clr(int pin){
	LCD_CTL &= ~pin;
}

/**************************************************************************************************
 * Obsluha interruptu
 *************************************************************************************************/
#pragma vector=USCIAB0TX_VECTOR		// I2C Data service routine (status je v USCIAB0RX_VECTOR)
__interrupt void USCIAB0TX_ISR(void){

	if(UCB0STAT & UCSTTIFG){
		UCB0STAT &= ~UCSTTIFG;
		if(IFG2 & UCB0RXIFG){
			_i2c_bufpos = UCB0RXBUF;
		}
	}

	if(IFG2 & UCB0TXIFG){
		if(_i2c_bufpos < _i2c_txbuflen){
			UCB0TXBUF = *(_i2c_txbuf + _i2c_bufpos);
			_i2c_bufpos++;
		}
		else{
			UCB0TXBUF = 0x00;
		}
	}

	if(IFG2 & UCB0RXIFG){
		*(_i2c_rxbuf + _i2c_bufpos) = UCB0RXBUF;
		_i2c_bufpos++;
	}
}

#pragma vector=TIMERA0_VECTOR				// Obsluha TIMERA0 (ms timer), watchdog
__interrupt void TIMERA0_VECTOR_ISR(void){

	if(tim_ms_var > 0)		tim_ms_var--;
	if(tim_main_var > 0)	tim_main_var--;

	if(!(_MEM.status & MEM_STATUS_WDT_ERR)){

		if((_wdt_var<=1000)&&(_wdt_var>0))	{

			_wdt_var--;

			if(_wdt_var == 0){

				_wdt_var = 1000;

				_MEM.watchdog--;

				if(_MEM.watchdog == 0){
					_MEM.status |= MEM_STATUS_WDT_ERR;
				}
			}
		}
	}
}
