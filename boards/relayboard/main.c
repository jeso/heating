#include <msp430.h> 

/**************************************************************************************************
 * Definice
 *************************************************************************************************/
#define I2C_ADDR			0x61
#define LED_RED_PORT		P2OUT
#define LED_RED_PIN			0x01
#define LED_BLUE_PORT		P2OUT
#define LED_BLUE_PIN		0x02
#define TP_PORT				P1OUT
#define TP_0				0x01
#define TP_1				0x02

#define MEM_MAGIC				0x55
#define MEM_DEV_TYPE_RELAYS		0x02
#define MEM_STATUS_INIT_OK		0x01
#define	MEM_STATUS_CYCLE_OK		0x02
#define MEM_STATUS_WDT_ERR		0x10
#define	MEM_STATUS_OUTPUT_ERR	0x20
#define MEM_STATUS_RESET		0x80


typedef struct {
	/* 0x00 */	unsigned char	magic;			//	Musi byt 0x55 - detekce zarizeni

	/* 0x01 */	unsigned char	dev_type;		//	Typ desky
												//	0x01 = Sensor x 8
												//	0x02 = Rele x 8
												// 	0x03 = Tlacitka
												//	0x04 = Komunikace

	/* 0x02 */	unsigned char	watchdog;		//	Pokud dojde do nuly, spusti alarm

	/* 0x03	*/	unsigned char	status;			//	Status register
												// 	bit0 = Probehla inicializace pole
												// 	bit1 = Probehla alespon 1 smycka
												// 	bit2 = ..rezerva..
												// 	bit3 = ..rezerva..
												// 	bit4 = Chyba watchdogu
												//	bit5 = ..rezerva..
												//	bit6 = ..rezerva..
												//	bit7 = Vyvolat reset

	/* 0x04	*/	unsigned char	output_en;		//	Povoleni jednotlivych vystupu

	/* 0x05 */	unsigned char	tim_rst;		//	Reset casovace daneho vystupu (zacne znovu), auto-nulovani

	/* 0x06 */	unsigned char	reserved1;		//	Rezerva (zarovnani 1)
	/* 0x07 */	unsigned char	reserved2;		//	Rezerva (zarovnani 2)

		/*
		 * Funkce:
		 *  - 0x00 = fix 0
		 *	- 0x01 = fixni hodnota 0/1 (X)
		 *	- 0x02 = sepnout na x sekund jednorazove
		 *	- 0x03 = sepnout na x minut jednorazove
		 *	- 0x04 = sepnout na x hodin jednorazove
		 *	- 0x05 = spinat na x sekund a y sekund pauza
		 *	- 0x06 = spinat na x sekund a y minut pauza
		 *	- 0x07 = spinat na x sekund a y hoidn pauza
		 *	- 0x08 = spinat na x minut a y sekund pauza
		 *	- 0x09 = spinat na x minut a y minut pauza
		 *	- 0x0A = spinat na x minut a y hodin pauza
		 *	- 0x0B = spinat na x hodin a y sekund pauza
		 *	- 0x0C = spinat na x hodin a y minut pauza
		 *	- 0x0D = spinat na x hodin a y hodin pauza
		 */

	/* 0x08 */	unsigned char fun_sel[8];
	/* 0x10 */	unsigned char x[8];
	/* 0x18 */	unsigned char y[8];

	/* 0x20 */	unsigned char uptime_d;
	/* 0x21 */	unsigned char uptime_h;
	/* 0x22 */	unsigned char uptime_m;
	/* 0x23 */	unsigned char uptime_s;

} t_MEM;


/**************************************************************************************************
 * Globalni promenne
 *************************************************************************************************/
t_MEM _MEM;						//	Auto-init to 0
unsigned char*	_i2c_txbuf;		//	TX Buffer start pointer
unsigned char*	_i2c_rxbuf;		//	RX Buffer start pointer
unsigned char	_i2c_bufpos;	//	I2C Data pointer, auto-init to 0
const unsigned char	_i2c_txbuflen = 36;	//	TX Buffer length
const unsigned char	_i2c_rxbuflen = 36;	//	RX Buffer length
unsigned long int tim_ms_var;	//	Auto-decrementing variable 1
unsigned long int tim_main_var;	//	Hlavni promenna citace (sekundovy cyklus)
unsigned long int _wdt_var;		//	Watchdog varible (ms->s)

unsigned long int _var_x_sec[8];
unsigned long int _var_y_sec[8];

/**************************************************************************************************
 * Prototypy funkci
 *************************************************************************************************/
void init_memory();
void init_ports();
void init_clocks();
void init_i2c();
void init_ms_timer();
void led_red_blink_fast(unsigned char count);
void led_red_blink_slow(unsigned char count);
void sleep_ms(unsigned int msec);

/**************************************************************************************************
 * Main
 *************************************************************************************************/
int main(void) {
	unsigned int i;
	WDTCTL = WDTPW | WDTHOLD;	//	Zastavit watchdog
	_wdt_var = 1000;

	P2DIR |= LED_RED_PIN + LED_BLUE_PIN;		//	Vystup pro LED
	P2OUT &= ~(LED_RED_PIN + LED_BLUE_PIN);		//	Zapnout obe LED

	init_memory();		//	Inicializace hodnoty registru
	init_ports();		//	Inicializace portu
	init_clocks();		//	Inicializace hodin
	init_i2c();			//	Inicializace I2C periferie
	init_ms_timer();	//	Inicializace milisekundoveho casovace

	led_red_blink_slow(3);	//	3x blinkout cervenou LED

	while(1){

		// Hlavni programova smycka na 1s
		tim_main_var = 1000;

		// Pokud je pozadovat reset, pak resetovat (vyprsenim WDT)
		if(_MEM.status & MEM_STATUS_RESET)	WDTCTL = 0;

		// POkud doslo k chybe watchdogu nebo chybe vystupu, zapni cervenou LED
		if((_MEM.status & MEM_STATUS_WDT_ERR) || (_MEM.status & MEM_STATUS_OUTPUT_ERR))	LED_RED_PORT &= ~LED_RED_PIN;

		// Pokud nedoslo k chybe, vypni cervenou LED;
		else	LED_RED_PORT |= LED_RED_PIN;

		/*****************************************************************************************/

		// Uptime inkrementace
		_MEM.uptime_s++;
		if(_MEM.uptime_s >= 60)	{	_MEM.uptime_m++;	_MEM.uptime_s = 0;	}
		if(_MEM.uptime_m >= 60)	{	_MEM.uptime_h++;	_MEM.uptime_m = 0;	}
		if(_MEM.uptime_h >= 24)	{	_MEM.uptime_d++;	_MEM.uptime_h = 0;	}


		/*****************************************************************************************/

		// Reset casovace on-request
		for(i=0; i<8; i++){
			if( _MEM.tim_rst & (1<<i) ){
				_var_x_sec[i] = 0;
				_var_y_sec[i] = 0;
				_MEM.tim_rst &= ~(1<<i);
			}
		}

		/*****************************************************************************************/

		// Pokud jsou casovace nenastavene nebo dobehly, pak znovu nacasovat
		for(i=0; i<8; i++){
			if( (_var_x_sec[i] == 0) && (_var_y_sec[i] == 0) ){

				// Jednorazove funkce, pokud jsou dostupne
				if(_MEM.fun_sel[i] == 0x02) {	_var_x_sec[i] = _MEM.x[i];			_MEM.x[i] = 0;	}
				if(_MEM.fun_sel[i] == 0x03) {	_var_x_sec[i] = _MEM.x[i] * 60;		_MEM.x[i] = 0;	}
				if(_MEM.fun_sel[i] == 0x04) {	_var_x_sec[i] = _MEM.x[i] * 3600;	_MEM.x[i] = 0;	}

				// Nastaveni casovacu x, pokud vyprsely
				if((_MEM.fun_sel[i] == 0x05)||(_MEM.fun_sel[i] == 0x06)||(_MEM.fun_sel[i] == 0x07))		_var_x_sec[i] = _MEM.x[i];
				if((_MEM.fun_sel[i] == 0x08)||(_MEM.fun_sel[i] == 0x09)||(_MEM.fun_sel[i] == 0x0A))		_var_x_sec[i] = _MEM.x[i] * 60;
				if((_MEM.fun_sel[i] == 0x0B)||(_MEM.fun_sel[i] == 0x0C)||(_MEM.fun_sel[i] == 0x0D))		_var_x_sec[i] = _MEM.x[i] * 3600;

				// Nastaveni casovacu y, pokud vyprsely
				if((_MEM.fun_sel[i] == 0x05)||(_MEM.fun_sel[i] == 0x08)||(_MEM.fun_sel[i] == 0x0B))		_var_y_sec[i] = _MEM.y[i];
				if((_MEM.fun_sel[i] == 0x06)||(_MEM.fun_sel[i] == 0x09)||(_MEM.fun_sel[i] == 0x0C))		_var_y_sec[i] = _MEM.y[i] * 60;
				if((_MEM.fun_sel[i] == 0x07)||(_MEM.fun_sel[i] == 0x0A)||(_MEM.fun_sel[i] == 0x0D))		_var_y_sec[i] = _MEM.y[i] * 3600;
			}
		}

		// Pokud je casovac naplnen vyssi hodnotou nez je v registru, doslo asi k ponechani stare velke (hodinove) hodnoty, nutno smazat
		for(i=0; i<8; i++){
			if(((_MEM.fun_sel[i] == 0x05)||(_MEM.fun_sel[i] == 0x06)||(_MEM.fun_sel[i] == 0x07)) && (_var_x_sec[i] > (_MEM.x[i])		))	_var_x_sec[i] = _MEM.x[i];
			if(((_MEM.fun_sel[i] == 0x08)||(_MEM.fun_sel[i] == 0x09)||(_MEM.fun_sel[i] == 0x0A)) && (_var_x_sec[i] > (_MEM.x[i] *60) 	))	_var_x_sec[i] = _MEM.x[i] * 60;
			if(((_MEM.fun_sel[i] == 0x0B)||(_MEM.fun_sel[i] == 0x0C)||(_MEM.fun_sel[i] == 0x0D)) && (_var_x_sec[i] > (_MEM.x[i] *3600)	))	_var_x_sec[i] = _MEM.x[i] * 3600;

			if(((_MEM.fun_sel[i] == 0x05)||(_MEM.fun_sel[i] == 0x08)||(_MEM.fun_sel[i] == 0x0B))&&(_var_y_sec[i] > (_MEM.y[i])		))	_var_y_sec[i] = _MEM.y[i];
			if(((_MEM.fun_sel[i] == 0x06)||(_MEM.fun_sel[i] == 0x09)||(_MEM.fun_sel[i] == 0x0C))&&(_var_y_sec[i] > (_MEM.y[i] *60)	))	_var_y_sec[i] = _MEM.y[i] * 60;
			if(((_MEM.fun_sel[i] == 0x07)||(_MEM.fun_sel[i] == 0x0A)||(_MEM.fun_sel[i] == 0x0D))&&(_var_y_sec[i] > (_MEM.y[i] *3600)	))	_var_y_sec[i] = _MEM.y[i] * 3600;
		}

		// Nastav vystupy podle funkci
		for(i=0; i<8; i++){

			// Vystup zakazany
			if((_MEM.output_en & (1 << i)) == 0)	P4OUT &= ~(1 << i);

			// Vystup vypnuty
			if(_MEM.fun_sel[i] == 0x00)		P4OUT &= ~(1 << i);

			// Vystup natvrdo 0/1, pokud povoleno
			if( (_MEM.fun_sel[i] == 0x01) && (_MEM.output_en & (1 << i) ) ){
				if(_MEM.x[i])	P4OUT |= (1 << i);
				else			P4OUT &= ~(1 << i);
			}

			// Casovane funkce
			if((_MEM.fun_sel[i] >= 0x02) && (_MEM.output_en & (1 << i))){
				if(_var_x_sec[i] > 0)	P4OUT |=	(1 << i);
				else					P4OUT &=	~(1 << i);
			}

		}

		// Pokud jsou casovace nacasovane, pak je dekrementuj po sekundach, nejdrive X
				for(i=0; i<8; i++){
							if(_var_x_sec[i] > 0) _var_x_sec[i]--;
					else	if(_var_y_sec[i] > 0) _var_y_sec[i]--;
				}

		/*****************************************************************************************/

		_MEM.watchdog = 0x14;

		LED_BLUE_PORT &= ~LED_BLUE_PIN;
		sleep_ms(100);
		LED_BLUE_PORT |= LED_BLUE_PIN;
		sleep_ms(100);
		LED_BLUE_PORT &= ~LED_BLUE_PIN;
		sleep_ms(100);
		LED_BLUE_PORT |= LED_BLUE_PIN;

		while(tim_main_var);

	}

}

/**************************************************************************************************
 * Function definitions
 *************************************************************************************************/

void init_memory()
{
	_MEM.magic		|=	MEM_MAGIC;				// Magicke cislo :-)
	_MEM.dev_type 	|=	MEM_DEV_TYPE_RELAYS;	// Dev type sensory
	_MEM.watchdog	|=	0x14;					// Watchdog na maximum
	_MEM.status		|=	MEM_STATUS_INIT_OK;		// Pole inicializovane
	_MEM.output_en	 = 	0x00;					// Zakaz vsechny vstupy

	_i2c_txbuf = &_MEM.magic;
	_i2c_rxbuf = &_MEM.magic;
	_i2c_bufpos = 0;
}

void init_ports()
{

	/*
	 * Pin#	Pin name		SEL	DIR	OUT	REN
	 * -------------------------------------
	 * 1.0	TP0				0	1	0	0
	 * 1.1	TP1				0	1	0	0
	 * 1.2	NC				0	0	1	1
	 * 1.3	NC				0	0	1	1
	 * 1.4	NC				0	0	1	1
	 * 1.5	NC				0	0	1	1
	 * 1.6	NC				0	0	1	1
	 * 1.7	NC				0	0	1	1
	 * SUMMARY				x00	x03	xFC	xFC
	 *
	 * 2.0	LED_RED			0	1	1	0
	 * 2.1	LED_BLUE		0	1	1	0
	 * 2.2	NC				0	0	1	1
	 * 2.3	NC				0	0	1	1
	 * 2.4	NC				0	0	1	1
	 * 2.5	NC				0	0	1	1
	 * 2.6	XIN				1	0	0	0
	 * 2.7	XOUT			1	0	0	0
	 * SUMMARY				xC0	x03	x3F	x3C
	 *
	 * 3.0	NC				0	0	1	1
	 * 3.1	I2C_SDA			1	0	0	0
	 * 3.2	I2C_SCL			1	0	0	0
	 * 3.3	NC				0	0	1	1
	 * 3.4	NC				0	0	1	1
	 * 3.5	NC				0	0	1	1
	 * 3.6	NC				0	0	1	1
	 * 3.7	NC				0	0	1	1
	 * SUMMARY				x06	x00	xF9	xF9
	 *
	 * 4.0	OUT0			0	1	0	0
	 * 4.1	OUT1			0	1	0	0
	 * 4.2	OUT2			0	1	0	0
	 * 4.3	OUT3			0	1	0	0
	 * 4.4	OUT4			0	1	0	0
	 * 4.5	OUT5			0	1	0	0
	 * 4.6	OUT6			0	1	0	0
	 * 4.7	OUT7			0	1	0	0
	 * SUMMARY				x00	xFF	x00	x00
	 */

	P1SEL = 0x00;
	P1DIR = 0x03;
	P1OUT = 0xFC;
	P1REN = 0xFC;

	P2SEL = 0xC0;
	P2DIR = 0x03;
	P2OUT = 0x3F;
	P2REN = 0x3C;

	P3SEL = 0x06;
	P3DIR = 0x00;
	P3OUT = 0xF9;
	P3REN = 0xF9;

	P4SEL = 0x00;
	P4DIR = 0xFF;
	P4OUT = 0x00;
	P4REN = 0x00;
}

void init_clocks(){

	unsigned int i;

	__bic_SR_register(OSCOFF);	// Start oscillator

	BCSCTL1 |= XTS;				// High-freq. mode
	BCSCTL3 |= LFXT1S1;			// When in HF mode, then 3-16MHz oscillator

	while(1){							// Loop until done
		LED_RED_PORT &= ~LED_RED_PIN;	// Signal error state, until osc ok
		IFG1 &= ~OFIFG;					// Clear OFIFG
		for(i=0xFF; i>0; i--);			// Delay
		if(!(IFG1 & OFIFG))
			break;				// If OFIFG is clear (no osc. fault), quit
	}

	LED_RED_PORT |= LED_RED_PIN;	// Error state off

	BCSCTL1 = XT2OFF + XTS + DIVA_3 + RSEL0;	// XT2 Off, HF mode, ACLK = 1MHz
	BCSCTL2 = SELM_3 + DIVM_0 + SELS + DIVS_0;	// MCLK->LFXT1, MCLK=8MHz, SMCLK->LFXT1, SMCLK=8MHz
}

void init_i2c(){
	__bic_SR_register(GIE);			// Disable interrupts
	UCB0CTL1 |= UCSWRST;			// Reset the I2C Controller
	UCB0CTL0 = UCMODE_3 + UCSYNC;	// 7bit, single-master, slave, I2C, sync.
	UCB0CTL1 &= ~UCSWRST;			// Release I2C controller from reset
	UCB0I2COA = I2C_ADDR;			// Device address
	IE2 |= UCB0TXIE;
	IE2 |= UCB0RXIE;				// Enable RX/TX interrupts
	__bis_SR_register(GIE);			// Enable interrupts
}

void init_ms_timer(){
	TACTL 	|= 	TACLR;			// Stop and clear the timer
	TACCTL0 |= 	CCIE;			// Enable CCIFG IRQ
	TACTL 	= 	TASSEL_1 + ID_0 + MC_1;
	TACCR0 	= 	0x3E8;			// 1:1000 divider
}

// Zablika rychle (bez delaye)
void led_red_blink_fast(unsigned char count){
	unsigned char i;

	if(count == 0){
		while(1){
			LED_RED_PORT &= ~LED_RED_PIN;
			LED_RED_PORT |=  LED_RED_PIN;
		}
	}

	for(i=count; i>0; i--){
		LED_RED_PORT &= ~LED_RED_PIN;
		LED_RED_PORT |=  LED_RED_PIN;
	}
}

// Zablika pomalu (delaye)
void led_red_blink_slow(unsigned char count){
	unsigned char i;
	if(count == 0){
		while(1){
			LED_RED_PORT &= ~LED_RED_PIN;
			sleep_ms(100);
			LED_RED_PORT |=  LED_RED_PIN;
			sleep_ms(100);
		}
	}

	for(i=count; i>0; i--){
		LED_RED_PORT &= ~LED_RED_PIN;
		sleep_ms(100);
		LED_RED_PORT |=  LED_RED_PIN;
		sleep_ms(100);
	}

	sleep_ms(200);
}

//	Vycka uvedeny pocet milisekund
void sleep_ms(unsigned int msec){
	tim_ms_var = msec;
	while(tim_ms_var);
}

/**************************************************************************************************
 * Obsluha interruptu
 *************************************************************************************************/
#pragma vector=USCIAB0TX_VECTOR		// I2C Data service routine (status je v USCIAB0RX_VECTOR)
__interrupt void USCIAB0TX_ISR(void){

	if(UCB0STAT & UCSTTIFG){
		UCB0STAT &= ~UCSTTIFG;
		if(IFG2 & UCB0RXIFG){
			_i2c_bufpos = UCB0RXBUF;
		}
	}

	if(IFG2 & UCB0TXIFG){
		if(_i2c_bufpos < _i2c_txbuflen){
			UCB0TXBUF = *(_i2c_txbuf + _i2c_bufpos);
			_i2c_bufpos++;
		}
		else{
			UCB0TXBUF = 0x00;
		}
	}

	if(IFG2 & UCB0RXIFG){
		*(_i2c_rxbuf + _i2c_bufpos) = UCB0RXBUF;
		_i2c_bufpos++;
	}
}

#pragma vector=TIMERA0_VECTOR				// Obsluha TIMERA0 (ms timer), watchdog
__interrupt void TIMERA0_VECTOR_ISR(void){

	if(tim_ms_var > 0)		tim_ms_var--;
	if(tim_main_var > 0)	tim_main_var--;

	if(!(_MEM.status & MEM_STATUS_WDT_ERR)){

		if((_wdt_var<=1000)&&(_wdt_var>0))	{

			_wdt_var--;

			if(_wdt_var == 0){

				_wdt_var = 1000;

				_MEM.watchdog--;

				if(_MEM.watchdog == 0){
					_MEM.status |= MEM_STATUS_WDT_ERR;
				}
			}
		}
	}
}
